'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('MainCtrl', function ($scope, $location, $auth, $cookies, toastr, Account) {
  	console.log('main');

    /*if($auth.isAuthenticated){
      $location.path("/feed");
    };*/

    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function() {
          console.log('$auth', $auth.getToken()); 
          $cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
          toastr.success('You have successfully signed in with ' + provider + '!');
          var getProfile = function() {
            Account.getProfile()
              .then(function(response) {
                console.log('Account', response.data);
                if(response.data.new){
                  $location.path('/welcome');
                }
                else{
                  $location.path('/feed');
                  //$location.path('/@' + response.data.username);
                }
            })
            .catch(function(response) {
               toastr.error(response.data.message, response.status);
            });
          };
          getProfile(); 
          //$location.path('/feed');
        })
        .catch(function(error) {
          if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            toastr.error(error.error);
          } else if (error.data) {
            // HTTP response error from server
            toastr.error(error.data.message, error.status);
          } else {
            toastr.error(error);
          }
        });
    };

  });
