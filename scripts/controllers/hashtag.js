'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:HashtagCtrl
 * @description
 * # HashtagCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('HashtagCtrl', ['$scope', '$stateParams', '$auth', '$cookies', '$timeout', '$compile', '$location', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $stateParams, $auth, $cookies, $timeout, $compile, $location, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;
    var $body = $('body');

    $body.addClass('page-hashtag');

    $scope.hashtag = $scope.params['hashtag'];

    $body.removeClass('page-trends');
    $body.removeClass('open-profile');

    $scope.columns = [];
    $scope.walls = [];

    activate();

    function activate() {

      Column.get_hashtag($scope.hashtag).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
           if(data.data.success){
            $scope.columns.push(data.data.success[0])
           }
          console.log('get_hashtag', data);
      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    $scope.hashtagTo = function(hashtag){
      console.log("hashtag",hashtag);
      $location.path("/hashtag/"+ hashtag.name.toLowerCase());//.replace().reload(false)
    }


  }]);
