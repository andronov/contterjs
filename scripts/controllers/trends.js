'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:TrendsCtrl
 * @description
 * # TrendsCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('TrendsCtrl', ['$scope', '$stateParams', '$auth', '$cookies', '$timeout', '$compile', '$location', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $stateParams, $auth, $cookies, $timeout, $compile, $location, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;
    var $body = $('body');

    $body.addClass('page-trends');

    $body.removeClass('open-profile');


    $scope.userType = 'Anonymus';

    $scope.trends = true;

    //tokenSession
    //$cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
    //$scope.userType = 'Alien';
    //$scope.userType = 'Current';


    console.log('$scope.params', $scope.params);

    $scope.user = {};

    $scope.tester = function(data, id) {
      console.log(data, id);
    };

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          $scope.slugusername = $scope.user.username;
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();



    var vm = this;


    $scope.posts = [];

    $scope.columns = [];

    $scope.profiles = [];

    $scope.walls = [];
    //$scope.walls = [{'id': 1, 'name': 'one', 'icon': 'icon_w_17.png', 'current': true}, {'id': 2, 'name': 'two', 'icon': 'icon_w_17.png', 'current': false}, {'id': 3, 'name': 'three', 'icon': 'icon_w_17.png', 'current': false}];

    activate();


    //vm.columns = [{'column': {'id': 25, 'name': 'bbc'}, 'data': [{'id': 1, 'title': 'test1'}, {'id': 2, 'title': 'test2'}]}, {'column': {'id': 28, 'name': 'cnn'}, 'data': [{'id': 3, 'title': 'test3'}]}];

    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf
     */
    function activate() {

      Wall.trends().then(postsSuccessFn, postsErrorFn);

      /*$scope.$on('post.created', function (event, post) {
        vm.posts.unshift(post);
      });

      $scope.$on('post.created.error', function () {
        vm.posts.shift();
      });*/


      /**
       * @name postsSuccessFn
       * @desc Update thoughts array on view
       */
      function postsSuccessFn(data, status, headers, config) {
        console.log('trends', data.data);
        $scope.userType = data.data.userType;
        $scope.columns = data.data.columns;
        $scope.walls = data.data.walls;
        //$scope.posts = data.data;

      }


      /**
       * @name postsErrorFn
       * @desc Show snackbar with error
       */
      function postsErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        //Snackbar.error(data.error);
      }
    }


    $scope.showMenuColumn = function(el){
      console.log('showMenuColumn');
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if($header.hasClass('cntr-column-header-recontent') || $header.hasClass('cntr-column-header-favourite')){
        var clmn = null;

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == el){
            clmn = $scope.columns[i];
          }
        }

        clmn.launch = 'standart';
        return false
      }

      if($header.hasClass('cntr-column-header-open-set')){
        $scope.showColumnSettings(el);
      }
      $header.toggleClass('cntr-column-header-open-full');

    };

    /**
     * Switch walls
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    $scope.switchWall = function(el){
      var e = $(el.currentTarget),
          slug = e.data('slug'),
          wall = e.data('wall'),
          target = $(el.target);

      if(!e.hasClass('active-menu-wl')){
        $location.path("/@"+$scope.slugusername+"/"+ slug);//.replace().reload(false)
        $scope.$apply();
      }
      if(target.hasClass('menu-item-wall-top') || target.hasClass('icon-wall-top')){
        $scope.topWall(wall)
      }
      
    };

    $scope.ShowProfile = function($event){
      $location.path('/@'+ $scope.user.username)
    };
     



  }]);
