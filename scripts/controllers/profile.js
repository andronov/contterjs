'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:ProfileUserCtrl
 * @description
 * # ProfileUserCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('ProfileUserCtrl', function ($scope,$sce, $routeParams, $stateParams,  $http, $route, $location, $auth, $cookies, toastr, Account, Wall) {
    $scope.params = $stateParams;
    var username = $scope.params.user;
    $scope.user = {};
  	console.log('ProfileUserCtrl', $scope.params);
    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };

    $scope.getUser = function(username) {
      Account.getUser(username)
        .then(function(response) {
          console.log(response.data);
          if(response.data.success){
            $scope.user = response.data.data;
          }
          //$scope.user = response.data;
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getUser(username);

    $scope.walls = [];

    function activate() {
      Wall.all().then(postsSuccessFn, postsErrorFn);

      function postsSuccessFn(data, status, headers, config) {
        console.log(data.data.columns);
        $scope.columns = data.data.columns;
        $scope.walls = data.data.walls;
      }
      function postsErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    }
    activate();


    $scope.followUser = function($event) {
      var element = $($event.currentTarget);
      console.log('followUser', element);

      function handler(id, action){
        Account.followProfile(id, action)
        .then(function(response) {
          console.log(response.data);
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
      }

      if(!$scope.user.current){
        if(!$scope.user.follow){
          $scope.user.follow = true;
          handler($scope.user.id, 'follow');
        }
        else{
          $scope.user.follow = false;
          handler($scope.user.id, 'unfollow');
        }
      }
    }
});