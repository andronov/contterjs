angular.module('contterApp')
  .controller('LogoutCtrl', function($location, $auth, $cookies, toastr) {
    if (!$auth.isAuthenticated()) { return; }
    $auth.logout()
      .then(function() {
      	$cookies.remove('tokenSession');
        toastr.info('You have been logged out');
        $location.path('/');
      });
  });