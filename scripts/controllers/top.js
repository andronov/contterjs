'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:TopCtrl
 * @description
 * # TopCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('TopCtrl', ['$scope', '$stateParams', '$auth', '$cookies', '$timeout', '$compile', '$location', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $stateParams, $auth, $cookies, $timeout, $compile, $location, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;
    var $body = $('body');

    $body.addClass('page-top');

    $body.removeClass('open-profile');

    if($scope.params.category){
      $scope.cat = $scope.params.category.toUpperCase();
    }
    else{
      $scope.cat = 'RECOMMENDED';
    }
    


    $scope.userType = 'Anonymus';

    $scope.top = true;
    $scope.cat_id = null;

    //tokenSession
    //$cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
    //$scope.userType = 'Alien';
    //$scope.userType = 'Current';


    console.log('$scope.params', $scope.params, $scope.cat);

    $scope.user = {};

    $scope.tester = function(data, id) {
      console.log(data, id);
    };

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          $scope.slugusername = $scope.user.username;
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();



    var vm = this;


    $scope.posts = [];

    $scope.columns = [];

    $scope.categories = [];

    $scope.profiles = [];

    $scope.walls = [];

    activate();

    function get_columns() {

      Wall.top($scope.cat, $scope.cat_id).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('get_columnsget_columns', data);
          console.log(data.data.columns);

          $scope.userType = data.data.userType;
          $scope.columns = data.data.columns;
          $scope.walls = data.data.walls;
      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    
    

    function activate() {

      Wall.category('is_top').then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('categorycategory', data);
          $scope.categories = data.data.categories;

          for(var i = 0; i < $scope.categories.length; i++){
            console.log('.$scope.categories', $scope.categories[i])
            if($scope.categories[i].name == $scope.cat){
              $scope.cat_id = $scope.categories[i].id
            }
          }
          if(($scope.params.category && $scope.cat_id) || !$scope.params.category){
            get_columns();
          }
          else{
            console.log('nononononono')
          }
          

      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    


    $scope.showMenuColumn = function(el){
      console.log('showMenuColumn');
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if($header.hasClass('cntr-column-header-recontent') || $header.hasClass('cntr-column-header-favourite')){
        var clmn = null;

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == el){
            clmn = $scope.columns[i];
          }
        }

        clmn.launch = 'standart';
        return false
      }

      if($header.hasClass('cntr-column-header-open-set')){
        $scope.showColumnSettings(el);
      }
      $header.toggleClass('cntr-column-header-open-full');

    };

    /**
     * Switch walls
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    $scope.switchWall = function(el){
      var e = $(el.currentTarget),
          slug = e.data('slug'),
          wall = e.data('wall'),
          target = $(el.target);

      if(!e.hasClass('active-menu-wl')){
        $location.path("/@"+$scope.slugusername+"/"+ slug);//.replace().reload(false)
        $scope.$apply();
      }
      if(target.hasClass('menu-item-wall-top') || target.hasClass('icon-wall-top')){
        $scope.topWall(wall)
      }
      
    };

    $scope.switchTopCat = function($event, cat){
      var el = $($event.currentTarget),
          prt = el.parent(),
          name = el.data('name'),
          url = "/top"
      
      if(el.hasClass('active')){
        return false
      }

      prt.find('.nav-menu-item-top').removeClass('active');
      el.addClass('active');

      if(name!='RECOMMENDED'){
        url += "/" + name.toLowerCase()
      }

      $location.path(url)
    };


    $scope.ShowProfile = function($event){
      $location.path('/@'+ $scope.user.username)
    };

     



  }]);
