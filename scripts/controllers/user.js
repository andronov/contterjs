'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('UserCtrl', function ($scope, $location, $auth, $cookies, $stateParams, toastr, Account, Column, Wall) {
  	console.log('main');
    $scope.params = $stateParams;
    $scope.slugusername = $scope.params.username;

    /*if($auth.isAuthenticated){
      $location.path("/feed");
    };*/

    $scope.feeds = [];
    $scope.walls = [];
    $scope.columns = [];
    $scope.user = {};
    $scope.column = {};

    function get_walls(){
      Wall.get_walls($scope.slugusername).then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        $scope.walls = data.data.walls;
        console.log('get_walls', data.data);
      }
        function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

    function get_columns(){
      Column.get_columns($scope.slugusername).then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        $scope.columns = data.data.columns;
        console.log('get_columns', data.data);
      }
        function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };


    function activate() {

      get_walls();
      get_columns();

      Column.get_my_recontent().then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        
        $scope.feeds = data.data.recontent_items;
        console.log('get_feed_recontent', data.data, $scope.feeds);
      }
        function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
    };

    activate();

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          if(!$scope.slugusername){
            $scope.slugusername = $scope.user.username
          }

          
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();

    $scope.columnQuickView = function($event, column_id){
      var t =  $($event.currentTarget);
      

      if(t.hasClass('active')){
        t.removeClass('active');
        $('.c_user_column_item').removeClass('active');
        $scope.column = {};
        return false;
      }
      $('.c_user_column_item').removeClass('active');


      t.addClass('active');
      console.log("columnQuickView($event, column_id)", $event, column_id);


      Column.quickview($scope.slugusername, column_id).then(fnSuccessFn, fnErrorFn);
  
      function fnSuccessFn(data, status, headers, config) {
        $scope.column = data.data.column;
        console.log('quickview', data.data);
      }
        function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }

    };

    $scope.closeColumnQuickView = function($event){
      var t =  $($event.currentTarget);
      $('.c_user_column_item').removeClass('active');
      $scope.column = {};
    };


    $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
    };

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function() {
          console.log('$auth', $auth.getToken()); 
          $cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
          toastr.success('You have successfully signed in with ' + provider + '!');
          var getProfile = function() {
            Account.getProfile()
              .then(function(response) {
                console.log('Account', response.data);
                if(response.data.new){
                  $location.path('/welcome');
                }
                else{
                  $location.path('/feed');
                  //$location.path('/@' + response.data.username);
                }
            })
            .catch(function(response) {
               toastr.error(response.data.message, response.status);
            });
          };
          getProfile(); 
          //$location.path('/feed');
        })
        .catch(function(error) {
          if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            toastr.error(error.error);
          } else if (error.data) {
            // HTTP response error from server
            toastr.error(error.data.message, error.status);
          } else {
            toastr.error(error);
          }
        });
    };

    function resizeUserViewColumn() {
      console.log("resizeUserViewColumn()", $(window).width());
      var t = ($(window).width() - 400) / 2;
      $('.c_user_col_2').css("width", "400px");
      $('.c_user_col_1 , .c_user_col_3').css("width", t + "px");

    };

    $(window).resize(function(){
      resizeUserViewColumn()
    });

    resizeUserViewColumn();

    $scope.showMenuColumn = function($event){
      console.log('showMenuColumn');
      var t = $($event.currentTarget);
      var $header =t.parents('.cntr-column-header');

      $header.toggleClass('cntr-column-header-open-full');
    };

  });
