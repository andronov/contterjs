'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:LeftCtrl
 * @description
 * # LeftCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('LeftCtrl', ['$scope', '$rootScope', '$stateParams', '$auth', '$cookies', '$http', '$timeout', '$compile', '$location', '$route', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $rootScope, $stateParams, $auth, $cookies, $http, $timeout, $compile, $location, $route, toastr, Wall, Account, Column, MySocket) {
  var $body = $('body');
  $scope.params = $stateParams;

  $scope.slugwall = $scope.params.slugwall;
  $scope.slugusername = $scope.params.username;
  $scope.walls = [];
  $scope.user = null;
  $scope.wall_current = null;
  $scope.addwalls = [];
  $scope.columns = [];
  $scope.userType = 'Anonymus';

  console.log("LeftCtrl  init", $scope.$$nextSibling);

  $scope.isAuthenticated = function() {
      return $auth.isAuthenticated();
  };

  if($scope.isAuthenticated()){
    $cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
  }

  function activate(){
    Wall.get_walls($scope.slugusername, $scope.slugwall).then(fnSuccessFn, fnErrorFn);
  
    function fnSuccessFn(data, status, headers, config) {
      console.log('get_walls', data.data);

      $scope.walls = data.data.walls;
      $scope.userType = data.data.userType;

      for(var i = 0; i < $scope.walls.length; i++){
          if($scope.walls[i].current){
            $scope.wall_current = $scope.walls[i];
          }
        }


    }
      function fnErrorFn(data, status, headers, config) {
      console.log(data, status, headers, config);
    }
  }

  activate();

  $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          if(!$scope.slugusername){
            $scope.slugusername = $scope.user.username
          }

          
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
  };
  $scope.getProfile();

  /**
     * Switch walls
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    $scope.switchWall = function(el){
      var e = $(el.currentTarget),
          slug = e.data('slug'),
          wall = e.data('wall'),
          target = $(el.target);



      if(!e.hasClass('active-menu-wl')){
        $body.removeClass('open-top-wall');  
        $location.path("/@"+$scope.slugusername+"/"+ slug);//.replace().reload(false)
        //$scope.$apply();
        for (var i = 0; i < $scope.walls.length; i++) {
          if($scope.walls[i].slug == slug){
            $scope.walls[i].current = true;
            $scope.wall_current = $scope.walls[i];
          }
          else{
            $scope.walls[i].current = false;
          }
        };

      }
      if(target.hasClass('menu-item-wall-top') || target.hasClass('icon-wall-top')){
        $scope.$$nextSibling.topWall(wall)
      }
      
    };

    /**
     * [addColumn description]
     * @param {[type]} e [description]
     */
    $scope.addColumn = function($event){
      var el = $($event.currentTarget);

      el.toggleClass('active');

      console.log('column');
      $body.toggleClass('open-add-column');
      $('.column.column-add-column').removeClass('column-add-frommini-active');
      $('.column-panel-list.word').removeClass('active');
      $('.column.column-add-column').toggleClass('active');
      var w = '-405px'
      if($('.column.column-add-column').hasClass('active')){
          w = '0px'
      }
      if(!$('.wall-add-column').hasClass('actives')){
        $('.column.column-add-column').animate(
                {'margin-left': w}, {
                easing: null,
                duration: 1500,
                complete: function() {
                }
        });
      }
    };

    $scope.addGroupClose = function($event){
      var t = $($event.currentTarget);
      console.log(t, 'addGroupClose');
      $('.tooltip-add-wall').removeClass('active')
      $timeout(function () {
          $('.tooltip-add-wall').remove();
          $('.tooltip-add-wall-color').remove();
          $body.removeClass('add-wall');
      }, 310);
      
      
      
    };



    $scope.addGroupSort = function($event){
      var el = $($event.currentTarget),
          wall = el.parents('.lrw-wall'),
          wall_id = wall.data('id');
      console.log(el, 'addGroupSort');
      if(el.hasClass('dot')){

        /*el.on('mousedown', function() {
          console.log('w', wall);
          wall.addClass('draggable').on('mousemove', function(e) {
            $('.draggable').offset({
                top: e.pageY - $('.draggable').outerHeight() / 2,
                left: e.pageX - $('.draggable').outerWidth() / 2
            }).on('mouseup', function() {
                wall.removeClass('draggable');
            });
          });
        });*/
      }

      var addwalls = null,
          addwalls_prev = null,
          addwalls_next = null,
          sorter = [],
          addwalls_length = $scope.walls.length - 1,
          addwalls_id_prev = $(wall).prev().data('id'),
          addwalls_id_next = $(wall).next().data('id');

      for(var i = 0; i < $scope.walls.length; i++){
        if($scope.walls[i].id == wall_id){
          addwalls = $scope.walls[i];
        }
        if($scope.walls[i].id == addwalls_id_prev){
          addwalls_prev = $scope.walls[i];
        }
        if($scope.walls[i].id == addwalls_id_next){
          addwalls_next = $scope.walls[i];
        }

      }

      console.log('addwalls', addwalls);

      if(el.hasClass('left')){
        if(addwalls.sort > 1){
          addwalls_prev.sort = addwalls.sort;
          addwalls.sort -= 1;
        }
      }
      else if(el.hasClass('right')){
        if(addwalls.sort <= addwalls_length){
          addwalls_next.sort = addwalls.sort;
          addwalls.sort += 1;
        }
      }

      

      for(var i = 0; i < $scope.walls.length; i++){
        sorter.push({'wall': $scope.walls[i].id, 'sort': $scope.walls[i].sort})
      }


      function addsettings(types, wall_id, value){
        Wall.settings(types, wall_id, value).then(WallAddSuccessFn, WallAddErrorFn);
  
        function WallAddSuccessFn(data, status, headers, config) {console.log('settings wall sort', data);}

        function WallAddErrorFn(data, status, headers, config) {toastr.error(data.error);}
      }

      
    };
    /**
     * [addWall description]
     * @param {[type]} e [description]
     */
    $scope.addGroup = function(){
      console.log('group');
      //$('body').removeClass('open-profile open-profile-animate');
      //$('.column.column-add-column').removeClass('active');
      $body.toggleClass('add-wall');

      var wall_current = null;
      console.log('$scope.walls', $scope.walls);
      for(var i = 0; i < $scope.walls.length; i++){
        if($scope.walls[i].current){
          wall_current = $scope.walls[i];
          $scope.wall_current = $scope.walls[i];
        }
      }

      console.log('wall_current', wall_current);


      var tooltip = '<div id="tooltips" class="tooltip-add-wall "><div class="tooltip-content"><add-wall-html add-group-sort="addGroupSort" add-group-close="addGroupClose" addwalls="walls"></add-wall-html></div></div>';

      $body.append($compile(tooltip)($scope));
      $scope.$apply();


      $scope.addwalls = $scope.walls;
        
        $timeout(function () {
        handler()
      }, 500);
   
      function resizeForText(thiss, text) {
        var $this = thiss;
        var $span = $this.siblings('span.lrw-wall-st-name-span');
        $span.text(text);
        var $inputSize = $span.width() + 20;
        $this.css("width", $inputSize);
      }

      function handler(){
        
        tooltip = $('.tooltip-add-wall');
        //$compile(tooltip)($scope)
        tooltip.find('.add-wall-color').click(function(e){
          $scope.colorAddWall(e)
        });

        $timeout(function () {
          tooltip.addClass('active')
        }, 10);

        tooltip.find('.add-input-wall').on("keyup paste", function (event) {
          var prt = $(this).parents('.add-wall-group');
          this.value = this.value.replace(/[^0-9a-zA-Zа-яА-Я]/g,'');
          prt.find('.add-wall-form-icon span').html($(this).val())
        });

        tooltip.find('.lrw-wall-st-name-toggle').click(function(e){
          var el = $(this),
              prt = el.parent(),
              wall = el.parents('.lrw-wall'),
              wall_id = wall.data('id'),
              value = wall.find('.lrw-wall-st-name-control').val();

          prt.toggleClass('active');
          if(el.hasClass('icon-pencil')){
            el.removeClass('icon-pencil').addClass('icon-check')
          }
          else{
            el.removeClass('icon-check').addClass('icon-pencil');
            addsettings('title', wall_id, value);

            for(var i = 0; i < $scope.walls.length; i++){
              if($scope.walls[i].id == wall_id){
                $scope.walls[i].name = value;
                $scope.walls[i].slug = value.toLowerCase();
                if(wall_current.id == wall_id && $scope.slugusername){
                  console.log('uess');

                  var original = $location.path;
                  var lastRoute = $route.current;
                  var un = $rootScope.$on('$locationChangeSuccess', function () {
                      $route.current = lastRoute;
                      un();
                  });

                  original.apply($location, ["/@"+$scope.slugusername+"/"+ value.toLowerCase()])
                }
              }
            }

          }
        });

        var lrwcontrolname = tooltip.find('.lrw-wall-st-name-control');
      
        lrwcontrolname.each(function(indx, element){
          resizeForText($(element, $(element).val()))
        });

        lrwcontrolname.on("keyup paste", function (event) {
          var $this = $(this);
          this.value = this.value.replace(/[^0-9a-zA-Zа-яА-Я]/g,'');
          resizeForText($this, $this.val())
        });

        tooltip.find('.lrw-wall-st-color').click(function(e){
          var el = $(this),
              prt = el.parents('.lrw-wall'),
              addwall = el.parents('.add-wall'),
              colorui = addwall.find('.add-wall-color-ul').last();

          

          if(!el.hasClass('active')){
            var tooltip1 = $('<div id="tooltips" class="tooltip-add-wall-color "><div class="arrow"></div><div class="tooltip-content"></div></div>');
            tooltip1.css({'top': (el.offset().top + 26), 'left': (el.offset().left - 45)});
            
            $body.append(tooltip1);
            var clone = colorui.clone();
            clone.find('.add-wall-color').removeClass('active').data('id', prt.data('id'));
            tooltip1.find('.tooltip-content').html(clone);
            el.addClass('active');

            tooltip1.find('.add-wall-color').click(function(e){
              var el = $(this),
                  color = el.data('color'),
                  prt = $(".lrw-wall[data-id='"+el.data("id")+"']"),
                  wall_id = prt.data('id');


              for(var i = 0; i < $scope.walls.length; i++){
                if($scope.walls[i].id == wall_id){
                  $scope.walls[i].color = color;
                }
              }

              prt.css({'background': color});
              addsettings('color', prt.data('id'), color);
              tooltip1.remove();
            });

          }
          else{
            $('.tooltip-add-wall-color').remove();
            el.removeClass('active');
          }

        });

        tooltip.find('.lrw-wall-st-home').click(function(e){
              var el = $(this),
                  wall = el.parents('.lrw-wall'),
                  wall_id = wall.data('id'),
                  addwalls = null;

            for(var i = 0; i < $scope.addwalls.length; i++){
              if($scope.addwalls[i].id == wall_id){
                addwalls = $scope.addwalls[i];
              }
              else{
                $scope.addwalls[i].home = false
              }
            }

            addwalls.home = true;
            $scope.$apply();

            addsettings('home', wall_id, true);
        });

        function RGBtoHEX(color) {
          return "#"+$.map(color.match(/\b(\d+)\b/g),function(digit){
            return ('0' + parseInt(digit).toString(16)).slice(-2)
          }).join('');
        };

        //add column click add-btn-column
        tooltip.find('.add-btn-column').click(function(e){
            var el = $(this),
                block = el.parents('.add-wall-group-block'),
                name = block.find('.add-input-wall').val(),
                color = block.find('.add-wall-form-icon').css('background-color'),
                c = RGBtoHEX(color),
                next = false,
                sort = $('.lrw-wall').length + 1;

            if(name.length<1 || name.length > 11){
              toastr.warning('Name of wall MINIMUM 1 symbol MAXIMUM 11 symbols');
              return false
            }
            if(c == '#d3e1f5'){
              toastr.warning('Choice color for your wall');
              return false
            }

            addhandler(name, c, sort);

            console.log('ff', RGBtoHEX(color), name, sort)
        });

        tooltip.find('.lrw-wall-st-delete-action').click(function(e){
            var el = $(this),
                  wall = el.parents('.lrw-wall'),
                  wall_id = wall.data('id'),
                  addwalls = null;

            for(var i = 0; i < $scope.addwalls.length; i++){
              if($scope.addwalls[i].id == wall_id && wall_current.id == wall_id){
                toastr.warning('You can not delete the current wall');
                return false
              }  
              else if($scope.addwalls[i].id == wall_id){
                $scope.addwalls.splice(i,1)
              }
            }

            for(var i = 0; i < $scope.walls.length; i++){
              if($scope.walls[i].id == wall_id && wall_current.id == wall_id){
                toastr.warning('You can not delete the current wall');
                return false
              }  
              else if($scope.walls[i].id == wall_id){
                $scope.walls.splice(i,1)
              }
            }
            $scope.$apply();

            addsettings('delete', wall_id, '')
           
        });

      }

      function addhandler(name, color, sort){
        Wall.create(name, color, sort).then(WallAddSuccessFn, WallAddErrorFn);
          //toastr.error('sdsd34234242342hd');
  
        function WallAddSuccessFn(data, status, headers, config) {
          console.log('create wall', data);
          if(data.data.failure){
            toastr.error(data.data.failure);
          }
          else{
            tooltip.remove();
            var nm = data.data.data.name;
            $location.path("/@"+$scope.slugusername+"/"+ nm.toLowerCase());
          }
        }
  
  
        function WallAddErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      }

      function addsettings(types, wall_id, value){
        console.log('addsettings', types, wall_id, value);
        Wall.settings(types, wall_id, value).then(WallAddSuccessFn, WallAddErrorFn);
  
        function WallAddSuccessFn(data, status, headers, config) {
          console.log('create wall', data);
        }
  
  
        function WallAddErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      }
    };

    $scope.colorAddWall = function($event){
      var e = $($event.currentTarget),
          color = e.data('color'),
          prt = e.parents('.add-wall-group');
      prt.find('.add-wall-form-icon').css({'background-color': color});
      prt.find('.add-wall-color').removeClass('active');
      e.addClass('active');
    }

    $scope.sendAddWall = function($event){
      var e = $($event.currentTarget);
      console.log('sendAddWall', e);
      var prt = e.parents('.form-group-add-wall');
      var name = prt.find('.input-add-column').val();
      var color = prt.find('.add-wall-color.active').data('color');

      console.log('sendAddWall', name, color);
      if(!name){
        toastr.error('No name');
        return false
      }

        //$scope.addColumn();
      Wall.create(name, color).then(WallAddSuccessFn, WallAddErrorFn);
        //toastr.error('sdsd34234242342hd');

      function WallAddSuccessFn(data, status, headers, config) {
        console.log('create wall', data);
        if(data.data.failure){
          toastr.error(data.data.failure);
        }
        else{
          $scope.addGroup(); 

          for(var i = 0; i < $scope.walls.length; i++){
            $scope.walls[i].current = false;
          }
          $scope.walls.push(data.data.data);
          $scope.columns = [];

          console.log("$scope.walls$scope.walls", $scope.walls)

          $location.path("/@"+$scope.slugusername+"/"+ data.data.data.name)
        }
      }


      function WallAddErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
      }
    }

    /****отслеживаем курсор,двигаем колонки***/
    $(window).mousemove(function(e){
      var w = $(window).width(),
          h = $(window).height(),
          x = e.pageX,
          y = e.pageY;

      if(x > (w-100) && !$body.hasClass('open-add-column')){
        var v = $('.block-container-columns.l-scroll').scrollLeft();
        $('.block-container-columns.l-scroll').scrollLeft(v + 35);
        //console.log(v, "X Axis : " + e.pageX + " Y Axis : " + e.pageY);
      }
      else if((w-(w-100))>x && !$body.hasClass('open-add-column')){
        var v = $('.block-container-columns.l-scroll').scrollLeft();
        $('.block-container-columns.l-scroll').scrollLeft(v - 35);
        //console.log( "Xgghgh Axis : " + e.pageX + " Y Axis : " + e.pageY);
      }
     
    });

    $scope.minimizeColumnMenu = function($event){
      var el = $($event.currentTarget),
          prt = el.parent();

      el.toggleClass('active');
      prt.toggleClass('mini');
    }

    $scope.columnMenuTopWall = function($event){
      var el = $($event.currentTarget);

      el.toggleClass('active');
      $rootScope.$emit('TopWallEmit', $scope.wall_current.id);
    }

    $scope.showFullMenu = function($event){
      var el = $($event.currentTarget),
          menu = $('.full-menu-block');

      el.toggleClass('open');
      menu.toggleClass('active');
    }

    $('.full-menu-block').mouseleave(function(){
      $(this).removeClass('active');
    });

    

    //get columns
    $rootScope.$on('childEmit', function(event, data) {
      $scope.columns = data;
    });


}]);
