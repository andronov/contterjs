'use strict';

/**
 * @ngdoc function
 * @name contterApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the contterApp
 */
angular.module('contterApp')
  .controller('FeedCtrl', ['$scope', '$rootScope', '$stateParams', '$auth', '$cookies', '$http', '$timeout', '$compile', '$location', '$route', 'toastr', 'Wall', 'Account', 'Column', 'MySocket', function ($scope, $rootScope, $stateParams, $auth, $cookies, $http, $timeout, $compile, $location, $route, toastr, Wall, Account, Column, MySocket) {
    $scope.params = $stateParams;
    var $body = $('body');

    $body.removeClass('open-profile').removeClass('page-top').removeClass('page-trends').removeClass('page-hashtag');
    $body.addClass('page-column')

    console.log("$scope.params", $scope.params);

    $scope.slugwall = $scope.params.slugwall;
    $scope.slugusername = $scope.params.username;

    $scope.userType = 'Anonymus';

    $scope.openTopColumns = false;
    $scope.top_column = false;

    //reloader
    $scope.reloaderwall = true;
    //$scope.reloaderwall = true;

    // show add left menu
    $scope.showleftmenu = true;

    console.log('location');
        // IF USER FIRST
    $scope.showfirstvisit = false;
    $scope.collect = [];
    $scope.selectwall = [];

    $scope.switchCatWelcome = function($event) {
      var el = $($event.currentTarget),
          id = el.data('id');
      for (var i = 0; i < $scope.collect.length; i++) {
        if($scope.collect[i].id == id){
          $scope.collect[i].active = true;
        }
        else{
          $scope.collect[i].active = false
        }
      };
    };

    $scope.selectWallWelcome = function($event) {
      var el = $($event.currentTarget),
          prt = el.parent(),
          id = prt.data('id');
      
      if($scope.selectwall.indexOf(id) == -1){
        $scope.selectwall.push(id);
        prt.addClass('selected')
      }
      else{
        console.log("selectWallWelcome", $scope.selectwall.indexOf(id));
        var ix = $scope.selectwall.indexOf(id);
        $scope.selectwall.splice(ix,1)
        prt.removeClass('selected')
      }
    };

    $scope.createWallWelcome = function($event) {
      var el = $($event.currentTarget);

      if(el.hasClass('active') && $scope.selectwall.length>=3){
        console.log('createWallWelcome', $scope.selectwall);
        el.addClass('activing');
        handler()
      }

      function handler(){
        var sw = [];
        for (var i = 0; i < $scope.collect.length; i++) {
          for (var h = 0; h < $scope.collect[i].walls.length; h++) {
            if($scope.selectwall.indexOf($scope.collect[i].walls[h].id) != -1){
              sw.push($scope.collect[i].walls[h])
            }
          }
        };
        Wall.welcome(sw).then(fnSuccessFn, fnErrorFn);

        function fnSuccessFn(data, status, headers, config) {     
          console.log('createWallWelcome', data.data);
          if(data.data.success){
            el.removeClass('activing');
            //$location.path("/feed");
          }                              
        }
        function fnErrorFn(data, status, headers, config) {toastr.error(data.error);} 
      }

    };
    

    if ($location.$$path == '/welcome'){

      $scope.showfirstvisit = true;

      $http({
        method: 'POST',
        url: '/api/v1/welcome/',
        data: {},
      }).success(function (data, status, headers, config) {
        console.log('welcome', data);
        $scope.collect = data.collect;
        // handle success things
      }).error(function (data, status, headers, config) {
        console.log('welcome err', data, status, headers, config)
        // handle error things
      });

      return false
    }



    //tokenSession
    //$cookies.put('tokenSession', 'Bearer ' + $auth.getToken());
    //$scope.userType = 'Alien';
    //$scope.userType = 'Current';


    console.log('$scope.params', $scope.params);
    
    $scope.ws_new_links = MySocket.ws_new_links;
    $scope.ws_new_related_links = MySocket.ws_new_related_links;

    $scope.user = {};

    $scope.tester = function(data, id) {
      console.log(data, id);
    };

    $scope.getProfile = function() {
      Account.getProfile()
        .then(function(response) {
          console.log('Account', response.data);
          $scope.user = response.data;
          if(!$scope.slugusername){
            $scope.slugusername = $scope.user.username
          }

          
          
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };
    $scope.getProfile();


    
    /* start user card */

    $scope.getUser = function(username, callback) {
      Account.getUser(username)
        .then(function(response) {
          console.log(response.data);
          if(response.data.success){
            $scope.account = response.data.data;
            if(callback){
              callback();
            }
          }
          //$scope.user = response.data;
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
    };

    // if only user in url
    if(!$scope.slugwall && $scope.slugusername){
      //$body.addClass('open-profile');
      $scope.getUser($scope.slugusername);
      $body.addClass('open-profile');

    }

    

    
    //$scope.getUser($scope.user.displayName);
    //console.log('$scope.user0,', $scope.account);

    /* end user card */


    /******shoe nofication*******/
    $scope.showNotificationColumn = function(column){
      console.log('showNotificationColumn', column);
      var $column = $('.column[data-column="'+column.id+'"]');
      $column.find('.cntr-pallet-noti').removeClass('active');

      var arr = [];
      for (var key in $scope.ws_new_links){
        if($scope.ws_new_links[key].column_id == column.id){
          console.log($scope.ws_new_links[key]);

          column.items.unshift($scope.ws_new_links[key].post);

          arr.push(key);
          console.log(column);
          //$scope.$apply();
        }
      };
      for(var i = 0; i < arr.length; i++){
        delete $scope.ws_new_links[arr[i]]
      }
    };


    /**** end show notification ******/


    console.log(Wall);
    var vm = this;

    $scope.iconsGroups = [];

    $scope.posts = [];

    $scope.columns = [];
    $scope.top_columns = {"items": []};

    $scope.profiles = [];

    $scope.walls = [];
    $scope.addwalls = [];

    $scope.recontents = {"proccess_my": false, "proccess": false, "active_my": false, "active": false, "if_check": false, "if_have": false, "if_check_my": false, "if_have_my": false, "my": true, "my_items": [], "feed_items": [], "items": []};

    activate();


    /**
     * @name activate
     * @desc Actions to be performed when this controller is instantiated
     * @memberOf
     */
    function activate() {

      Wall.filter($scope.slugusername, $scope.slugwall).then(postsSuccessFn, postsErrorFn);

      /*$scope.$on('post.created', function (event, post) {
        vm.posts.unshift(post);
      });

      $scope.$on('post.created.error', function () {
        vm.posts.shift();
      });*/


      /**
       * @name postsSuccessFn
       * @desc Update thoughts array on view
       */
      function postsSuccessFn(data, status, headers, config) {
        console.log("data.data", data);
        $scope.userType = data.data.userType;
        $scope.columns = data.data.columns;
        if(data.data.walls){
          $scope.walls = data.data.walls;
        }

        $rootScope.$emit('childEmit', $scope.columns);
        

        $scope.reloaderwall = false;

      }


      /**
       * @name postsErrorFn
       * @desc Show snackbar with error
       */
      function postsErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        //Snackbar.error(data.error);
      }
    }
     

    $scope.showOnIconWall = function($event){
      var t = $($event.currentTarget);
      var parent = $(t).parents('.add-icon-group-block');
      parent.find('.add-icon-group-block-icon').removeClass('active');
      t.addClass('active');
      console.log(t, 'showOnIconWall');
    }; 

    /**
     * Hover effect column -> active
     * @param  {this}
     * @return {none}
     */
    $scope.hoverColumnActive = function(el){
      //console.log('shdfgdflumn', el);

    	/*var el = $('.column[data-column="'+el+'"]');
    	var id = el.data('column');

    	$('.column').not(el).removeClass('active-clm');
    	el.addClass('active-clm');

    	$('.menu-item').not($('.menu-item[data-column="'+id+'"]')).removeClass('active-menu-clm');
    	$('.menu-item[data-column="'+id+'"]').addClass('active-menu-clm');*/
    };

    /**
     * Show sub-menu-column
     * @param  {this}
     * @return {none}
     */
    $scope.showMenuColumn = function(el){
      console.log('showMenuColumn');
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if($header.hasClass('cntr-column-header-recontent') || $header.hasClass('cntr-column-header-favourite')){
        var clmn = null;

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == el){
            clmn = $scope.columns[i];
          }
        }

        clmn.launch = 'standart';
        return false
      }

      if($header.hasClass('cntr-column-header-open-set')){
        $scope.showColumnSettings(el);
      }
      $header.toggleClass('cntr-column-header-open-full');

     /* $parent.find('.c-hamburger').toggleClass('is-active');

        //$parent.find('.column-header').css({'height': '250px'}).toggleClass('column-open-sub-menu');
        //$parent.find('.column-content').css({'top': '252px'}).toggleClass('column-open-sub-menu');
      $parent.find('.column-header').toggleClass('column-open-sub-menu');
      $parent.find('.column-content').toggleClass('column-open-sub-menu');*/
    };

    $scope.showColumnSettings = function(el){
      var $column = $('.column[data-column="'+el+'"]');
      var $header = $column.find('.cntr-column-header');

      if(!$header.hasClass('cntr-column-header-open-set')){
        $header.addClass('cntr-column-header-open-set');
        $timeout(function () {
          $header.addClass('cntr-column-header-open-set-vis');
        }, 200);
      }
      else{
        $header.removeClass('cntr-column-header-open-set-vis');
        $timeout(function () {
          $header.removeClass('cntr-column-header-open-set');
        }, 300);
      }
     /* $column.find('.cntr-column-header').toggleClass('cntr-column-header-open-set');

      $timeout(function () {
          $column.find('.cntr-column-header').toggleClass('cntr-column-header-open-set-vis');
      }, 500);*/
    };

    //add column
    /*$rootScope.$on('addColumnEmit', function(event, data) {
      $scope.addColumn()
    });*/

    /**
     * [addColumn description]
     * @param {[type]} e [description]
     */
    $scope.addColumn = function(){
        console.log('column');
        $('.column.column-add-column').removeClass('column-add-frommini-active');
        $('.column-panel-list.word').removeClass('active');


        $('.column.column-add-column').toggleClass('active');
          var w = '-405px'
          if($('.column.column-add-column').hasClass('active')){
            w = '0px'
          }

        if($('.wall-add-column').hasClass('actives')){
        

          

        }
        else{
          
          $('.column.column-add-column').animate(
                  {'margin-left': w}, {
                  easing: null,
                  duration: 1500,
                  complete: function() {
                  }
          });
        }

        
    };

    /** Меняем тип колонки при создании*/
    $scope.ChangeTypeAddColumn = function($event){
        console.log('ChangeTypeAddColumn', $event);
        var el = $($event.currentTarget),
            standart = $('[data-type="addstandart"]'),
            custom = $('[data-type="addcustom"]');

        standart.hide();
        custom.show();
    };

    $scope.AddColumnCustomClose = function($event){
        console.log('AddColumnCustomClose', $event);
        var el = $($event.currentTarget),
            standart = $('[data-type="addstandart"]'),
            custom = $('[data-type="addcustom"]');

        custom.hide();
        standart.show();
        
    };


    $scope.AddColumnWords = function($event){
        console.log('AddColumnWords');
        var el = $($event.currentTarget),
            letter = el.data('letter'),
            prt = el.parents('.addcolumn-block-words'),
            feeds = $('.addcolumn-block-feeds'),
            sites = new Array(),
            types = 'fromletter';

        //prt.hide();
        prt.addClass('slidehidden');
        
        $scope.addcolumns = [];

        function groupBy( array , f ){
          var groups = {};
          array.forEach( function( o )
          {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
          });
          return Object.keys(groups).map( function( group )
          {
            return groups[group]; 
          })
        };

        var feed = '<add-column-html a-show-rss-feeds="AShowRssFeeds" more-column-from-list="MoreColumnFromList" add-column-from-user="AddColumnFromUser" add-column-from-list="addColumnFromList" add-column-back-feeds="AddColumnBackFeeds" letter="letter" addcolumnsres="addcolumnsres" addcolumns="addcolumns"></add-column-html>';

        Column.get_letters(letter, types).then(ColumnSuccessFn, ColumnErrorFn);

        function ColumnSuccessFn(data, status, headers, config) {
          console.log('get_letters', data.data.data);
          var d = data.data.data;
          var res = data.data.failure ? false : true;
          for(var i = 0; i < d.length; i++){
            var sh = d[i].short_url;
            var s = {'short_url': sh, 'data': d[i]};
            sites.push(s);
          }

          var result = groupBy(sites, function(item){return [item.short_url];});

          $scope.addcolumns = result;
          $scope.addcolumnsres = res;
          $scope.letter = letter;

          feeds.html($compile(feed)($scope));
          
          //feeds.show();                                     
        }


        function ColumnErrorFn(data, status, headers, config) {toastr.error(data.error);} 

        feeds.addClass('slideopened');
                
    };

    /**
    * из инпута ввода
    */
    $scope.AddInColumnSearchWords = function($event){
        console.log('AddInColumnSearchWords', $event);
        var el = $($event.currentTarget),
            prt = el.parents('.addcolumn-header-standart'),
            val = prt.find('.input-add-column').val();

        if(val.length>0){
          $scope.AddColumnSearchWords(val);
        }      
    };
    $scope.AddColumnSearchWords = function(value){
        console.log('AddColumnSearchWords', value);

        var letter = value,
            prt = $('.addcolumn-block-words'),
            feeds = $('.addcolumn-block-feeds'),
            sites = new Array(),
            types = 'fromsearch';

        prt.addClass('slidehidden');

        if(letter.length<=0){
          feeds.find('.addcolumn-block-feed-back').click();
          return false
        }
        
        $scope.addcolumns = [];

        function groupBy( array , f ){
          var groups = {};
          array.forEach( function( o )
          {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
          });
          return Object.keys(groups).map( function( group )
          {
            return groups[group]; 
          })
        };

        var feed = '<add-column-html more-column-from-list="MoreColumnFromList" add-column-from-user="AddColumnFromUser" a-show-rss-feeds="AShowRssFeeds" add-column-back-feeds="AddColumnBackFeeds" letter="letter" addcolumnsres="addcolumnsres" addcolumns="addcolumns"></add-column-html>';

        Column.get_letters(letter, types).then(ColumnSuccessFn, ColumnErrorFn);

        function ColumnSuccessFn(data, status, headers, config) {
          console.log('get_letters', data.data.data);
          var d = data.data.data;
          var res = data.data.failure ? false : true;
          for(var i = 0; i < d.length; i++){
            var sh = d[i].short_url;
            var s = {'short_url': sh, 'data': d[i]};
            sites.push(s);
          }

          var result = groupBy(sites, function(item){return [item.short_url];});

          $scope.addcolumns = result;
          $scope.addcolumnsres = res;
          $scope.letter = letter;

          feeds.html($compile(feed)($scope));


        }

        function ColumnErrorFn(data, status, headers, config) {toastr.error(data.error);} 

        feeds.addClass('slideopened');
                
    };

    $scope.AddColumnBackFeeds = function($event){
        console.log('AddCOlumnBackFeeds');
        var el = $($event.currentTarget),
            prt = $('.addcolumn-block-feeds'),
            words = $('.addcolumn-block-words');

        prt.removeClass('slideopened');
        words.removeClass('slidehidden');
        
    };

    $scope.MoreColumnFromList = function($event){
        var el = $($event.currentTarget),
            elems = [],
            letter = $('.addcolumn-block-feed-letter').html();
        console.log("MoreColumnFromList", el, $scope.addcolumns);

        function groupBy( array , f ){
          var groups = {};
          array.forEach( function( o )
          {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
          });
          return Object.keys(groups).map( function( group )
          {
            return groups[group]; 
          })
        };

        for (var i = 0; i < $scope.addcolumns.length; i++) {
          for (var g = 0; g < $scope.addcolumns[i].length; g++) {
            elems.push($scope.addcolumns[i][g].data.id)
          };
        };

        if(!el.hasClass('show-more-waiting') && !el.hasClass('show-more-stop')){
          el.addClass('show-more-waiting');
          Column.show_more_sites(elems, letter).then(ColumnSuccessFn, ColumnErrorFn);

          function ColumnSuccessFn(data, status, headers, config) {
            console.log('show_more_sites', data.data);
            var st = [],
                d = data.data.data;
            if(data.data.data.length>0){
              for(var i = 0; i < d.length; i++){
                var sh = d[i].short_url;
                var s = {'short_url': sh, 'data': d[i]};
                st.push(s);
              }

              var result = groupBy(st, function(item){return [item.short_url];});
              for (var i = 0; i < result.length; i++) {
                $scope.addcolumns.push(result[i]);
              };
               
               el.removeClass('show-more-waiting');
            }
            else{
              el.removeClass('show-more-waiting').addClass('show-more-stop');
            }
          }
  
          function ColumnErrorFn(data, status, headers, config) {
            el.removeClass('show-more-waiting').addClass('show-more-stop');
            toastr.error(data.error);
          } 
        }
        
    };

    $scope.AddColumnBackFeeds = function($event){
        console.log('AddCOlumnBackFeeds');
        var el = $($event.currentTarget),
            prt = $('.addcolumn-block-feeds'),
            words = $('.addcolumn-block-words');

        prt.removeClass('slideopened');
        words.removeClass('slidehidden');
        
    };

    $scope.AddColumnFromUser = function($event){
        var el = $($event.currentTarget),
            prt = el.parents('.addcolumn-add'),
            block_err = prt.find('.addcolumn-add-block-error'),
            block_s = prt.find('.addcolumn-add-block-success'),
            input = prt.find('input'),
            value = input.val();

        block_err.html("");
        block_s.html("");    
        function er(text){
          return '<p>'+text+'</p> <i class="icon icon-cancel-7"></i>'
        }

        block_err.unbind().click(function(){
          $(this).html("")
        });
        block_s.unbind().click(function(){
          $(this).html("")
        });

        if(value.length>0){
          Column.add_site(value).then(FnSuccessFn, fnErrorFn);
          function FnSuccessFn(data, status, headers, config) {
            console.log('add_site', data.data);
            if(data.data.success){
              block_s.html(er('Спасибо! Сайт успешно добавлен.'))
            }
            else if(data.data.failure == 'has already'){
              block_err.html(er('Вы уже добавили этот сайт.'))
            }
            else if(data.data.failure == 'too many today'){
              block_err.html(er('Превыше лимит в 20 сайтов в день.'))
            }
            input.val('');

            $timeout(function () {
              //block_err.html("");
             // block_s.html("");  
            }, 3000);

          }
          function fnErrorFn(data, status, headers, config) {
            console.log('add_site fnErrorFn', data, status, headers, config);
            //toastr.error(data.error);
          } 
        }

        console.log('AddColumnFromUser', prt);
        
    };

    $scope.AShowRssFeeds = function($event){
        console.log('AShowRssFeeds');
        var el = $($event.currentTarget),
            pallet = el.parents('.addcolumn-block-pallet'),
            txt = el.find('span'),//border-color:  transparent transparent #cc0000 transparent;
            color = pallet.data('color');

        if(pallet.hasClass('mini')){
          pallet.removeClass('mini').addClass('full');
          txt.html('HIDE RSS FEEDS');
          el.find("i").css({"border-color": "transparent transparent "+color+" transparent"});
          pallet.find('.media-list-body-dots').css({"background": color});
          pallet.find('.mini-column-add').css({"border-color": color, "color": color});
        }
        else{
          pallet.removeClass('full').addClass('mini');
          txt.html('SHOW RSS FEEDS');
          el.find("i").css({"border-color": ""+color+" transparent transparent  transparent"})
        }  
    };

    /*var $input = $('.input-word'),
    $buffer = $input.siblings('.input-buffer-word');
    $input.on('input', function(e) {
      $buffer.text($(this).val());
      $(this).width($buffer.width());
    });*/
    $scope.showOnIcon = function($event){
      var t = $($event.currentTarget);
      var parent = $(t).parents('.add-icon-group-block');
      parent.find('.add-icon-group-block-icon').removeClass('active');
      t.addClass('active');
      console.log(t, 'showOnIcon');
    };


    $scope.showOnWall = function($event){
      var t = $($event.currentTarget);
      var parent = $(t).parents('.add-source-group-block');
      var extra = parent.find('.add-source-group-block-div-extra');
      var elements = [];
      extra.html(' ');
      console.log(t, 'showOnSource');
      if(t.hasClass('icon-plus')){
        t.removeClass('icon-plus').addClass('icon-cancel');
        parent.find(".add-source-group-block-wall").each(function(indx, element){
          elements.push($(element).data('id'));
        });
        Column.get_settings_w_walls().then(ColumnSuccessFn, ColumnErrorFn);
        function ColumnSuccessFn(data, status, headers, config) {
          console.log('get_settings_w_walls', data.data.data);
          var d = data.data.data;
          for(var i = 0; i < d.length; i++){
            var pos = elements.indexOf(d[i].id);
            if(pos == -1) {
              extra.append('<div ng-click="showOnWallElement($event)" style="margin-right: 5px;border-color: rgba(0, 0, 0, 0.39); background: '+d[i].color+';" data-id="'+d[i].id+'" class="add-source-group-block-wall dop active " >'+d[i].name+'</div>');
            }   
          }; 
          $compile(extra)($scope);                                                
        }
        function ColumnErrorFn(data, status, headers, config) {toastr.error(data.error);} 
      }else{
        t.removeClass('icon-cancel').addClass('icon-plus');
      }
    };
    
    /* add wall to settings */
    $scope.showOnWallElement = function($event){
      var t = $($event.currentTarget);
      var e = $($event.currentTarget);
      var parent = $(t).parents('.add-source-group-block');

      if(!parent.parents('.column').hasClass('column-add-column')){
        $scope.settingsEditWallsWColumn(e)
      }
      else{
        if(t.parent().hasClass('add-source-group-block-div-extra')){
          parent.find('.add-source-group-block-divs-wall').prepend(t.clone().removeClass('extra'));
          t.remove();
          parent.find('.icon-cancel').removeClass('icon-cancel').addClass('icon-plus');
          parent.find('.add-source-group-block-div-extra').html('');
          $compile(parent.find('.add-source-group-block-divs-wall .add-source-group-block-wall.dop'))($scope);
        }
        else{
          t.remove();
        }
      }


      
      

    };

    /* edit wall to settings */
    $scope.settingsEditWallsWColumn = function(element){
      console.log('settingsEditWallsWColum', element);
      var action = 'add';
      var parent = $(element).parents('.add-source-group-block');
      var column_id = $(element).parents('.column').data('column');
      var wall_id = $(element).data('id');
      var current_id = $('.menu-item-wall.active-menu-wl').data('wall');
      var column_settings = $(element).parents('.column').find('.setting-sub-column');
      var clmn = null;

      for(var i = 0; i < $scope.columns.length; i++){
        if($scope.columns[i].id == column_id){
          clmn = $scope.columns[i];
          parent.find('.icon-cancel').removeClass('icon-cancel').addClass('icon-plus');
          parent.find('.add-source-group-block-div-extra').html('');
        }
      }

      //column_settings.find('.add-source-group-block-wall.dop.active[data-id="'+wall_id+'"]').remove();
      console.log('clmn', clmn);

      if($(element).hasClass('ng-scope')){
        action = 'delete'
      }
      if(column_id){

        

        Column.settings_w_walls(action, column_id, wall_id, current_id).then(fnSuccessFn, fnErrorFn);

        function fnSuccessFn(data, status, headers, config) {
          console.log('settings_w_walls', data);
          if(data.data.success){
            clmn.column_walls = data.data.data;
          }
        }
        function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      } 

    };

   /* $scope.showOnWallElement = function($event){
      var t = $($event.currentTarget);
      var parent = $(t).parents('.add-source-group-block');
      parent.find('.add-source-group-block-div > .add-source-group-block-img.current').after(t.clone().removeClass('extra'));
      t.remove();
      console.log(t, 'showOnWallElement');
    };*/

    $scope.settingsEditWordsWColumn = function($event, action){
      var element = $($event.currentTarget);
      var parent = element.parents('.add-source-group-block');
      var word_parents = parent.find('.add-words-group-block');
      var column_parents = element.parents('.column');
      var column_id = column_parents.data('column');
      console.log('settingsEditWordsWColumn', column_parents);
      var next = false,
          actions = 'add',
          types = 'matching',
          words = [];
      if(action == 'delete'){
        console.log('del', parent);
        parent.find('input').val('').keyup();
        element.parent().remove();
        next = true;
        actions = 'delete';
      }
      else if(action == 'add'){
        console.log('add', parent);
        parent.find('.add-source-group-block-word.dop').removeClass('acs-load').removeClass('dop');
        parent.find('input').val('').keyup();
        next = true;
      }

      if(!parent.hasClass('matching')){
        types = 'excluding'
      }

      word_parents.find(".add-source-group-block-word").each(function(indx, element){
        words.push($(element).find('span').html().toUpperCase())
      });

      
      if(!column_parents.hasClass('column-add-column') && next){

        Column.settings_word(column_id, words, types, actions).then(fnSuccessFn, fnErrorFn);
        //toastr.error('sdsd34234242342hd');

        function fnSuccessFn(data, status, headers, config) {
          console.log('settings_word', data);
        }
        function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      }
      
      

    };

    $scope.showOnSource = function($event){
      var t = $($event.currentTarget);
      var parent = $(t).parents('.add-source-group-block');
      var extra = parent.find('.add-source-group-block-div-extra');
      var elements = [];
      extra.html(' ');
      console.log(t, 'showOnSource');
      if(t.hasClass('icon-plus')){
        t.removeClass('icon-plus').addClass('icon-cancel');
        parent.find(".add-source-group-block-img").each(function(indx, element){
          elements.push($(element).data('id'));
        });
        Column.get_settings_w_source().then(ColumnSuccessFn, ColumnErrorFn);
        function ColumnSuccessFn(data, status, headers, config) {
          console.log('get_settings_w_source', data.data.data);
          var d = data.data.data;
          for(var i = 0; i < d.length; i++){
            var pos = elements.indexOf(d[i].id);
            if(pos == -1) {
              extra.append('<div ng-click="showOnSourceElement($event)" style="border-color: '+d[i].color+';" data-id="'+d[i].id+'" class="add-source-group-block-img extra active" ><img height="16" width="16" src="'+d[i].icon+'"></div>');
            }   
          }; 
          $compile(extra)($scope);                                                
        }
        function ColumnErrorFn(data, status, headers, config) {toastr.error(data.error);} 
      }else{
        t.removeClass('icon-cancel').addClass('icon-plus');
      }
    };
    $scope.showOnSourceElement = function($event){
      var t = $($event.currentTarget);
      console.log(t, 'showOnSourceElement');

      if(!t.parents('.column').hasClass('column-add-column')){
        $scope.settingsEditSourcesColumn(t)
      }

      if(t.hasClass('extra') && t.parents('.column').hasClass('column-add-column')){
        var parent = $(t).parents('.add-source-group-block');
        parent.find('.add-source-group-block-div.source').prepend(t.clone().removeClass('extra'));
        t.remove();
        parent.find('.icon-cancel').removeClass('icon-cancel').addClass('icon-plus');
        parent.find('.add-source-group-block-div-extra').html('');
        $compile(parent.find('.add-source-group-block-div.source .add-source-group-block-img'))($scope);  
      }
      else{
        /*if(t.parent.hasClass('add-source-group-block-div-extra') || t.parents('.column').hasClass('column-add-column')){
          t.remove();
        }*/
        t.remove();
        
      }
      
    };

    /* edit source to settings */
    $scope.settingsEditSourcesColumn = function(element){
      console.log('settingsEditSourcesColumn', element);
      var action = 'add';
      var parent = $(element).parents('.add-source-group-block');
      var column_id = $(element).parents('.column').data('column');
      var current_id = $(element).data('id');
      //var column_settings = $(element).parents('.column').find('.setting-sub-column');
      var clmn = null;

      for(var i = 0; i < $scope.columns.length; i++){
        if($scope.columns[i].id == column_id){
          clmn = $scope.columns[i];
          //parent.find('.icon-cancel').removeClass('icon-cancel').addClass('icon-plus');
          //parent.find('.add-source-group-block-div-extra').html('');
        }
      }

      //column_settings.find('.add-source-group-block-wall.dop.active[data-id="'+wall_id+'"]').remove();
      console.log('clmn', clmn);

      if($(element).hasClass('ng-scope')){
        action = 'delete'
      }
      if(column_id){

        

        Column.settings_w_source(action, column_id, current_id).then(fnSuccessFn, fnErrorFn);

        function fnSuccessFn(data, status, headers, config) {
          console.log('settings_w_source', data);
          if(data.data.success){
            clmn.source_columns = data.data.data;
          }
        }
        function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      } 

    };

    
    
    
    /**
     * Switch walls
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    $scope.switchWall = function(el){
      var e = $(el.currentTarget),
          slug = e.data('slug'),
          wall = e.data('wall'),
          target = $(el.target);

      if(!e.hasClass('active-menu-wl')){
        $location.path("/@"+$scope.slugusername+"/"+ slug);//.replace().reload(false)
        $scope.$apply();
      }
      if(target.hasClass('menu-item-wall-top') || target.hasClass('icon-wall-top')){
        $scope.topWall(wall)
      }
      
    };

    $scope.typeCClass = "checked-site";
    $scope.typeCColumn = "form-group-link";
    $scope.typeCValue = 'site';


    $scope.changeTypeOfColumn = function(){
          //console.log(Column.create());
          if ($scope.typeCClass === "checked-site"){
            $scope.typeCClass = "checked-word";
            $scope.typeCColumn = "form-group-word";
            $scope.typeCValue = 'word'
          }
          else{
            $scope.typeCClass = "checked-site";
            $scope.typeCColumn = "form-group-link";
            $scope.typeCValue = 'site'
            }
            
    };

    $scope.typeCClassForm = "checked-site";

    $scope.changeTypeOfColumnForm = function($event){
          if ($scope.typeCClassForm === "checked-site"){
            $scope.typeCClassForm = "checked-word";
          }
          else{
            $scope.typeCClassForm = "checked-site";
            }

      var e = $($event.currentTarget);
      var w = $('.cc-add-column.standart');
      var c = $('.cc-add-column.collection');
      if(e.hasClass('a__create__cl')){
        w.removeClass('active');c.addClass('active');
        //e.prev().removeClass('active');
        e.removeClass('a__create__cl');
      }
      else{
        c.removeClass('active');w.addClass('active');
        //e.next().removeClass('active');
        e.addClass('a__create__cl');
      }
    };

    $scope.ShowAddColumn = function($event){
      console.log('ShowAddColumn', $event);
      var e = $($event.currentTarget);
      var w = $('.cc-add-column.word');
      var c = $('.cc-add-column.collection');
      if(e.hasClass('a__create__cl')){
        w.removeClass('active');c.addClass('active');
        e.prev().removeClass('active');
        e.addClass('active');
      }
      else{
        c.removeClass('active');w.addClass('active');
        e.next().removeClass('active');
        e.addClass('active');
      }
      /*if(!e.hasClass('active')){
        e.addClass('active');
      }*/
    };

    $scope.addColumnFromList = function(id){
      console.log('addColumnFromList', id);
      var data = {};
      data['types'] = 'frommini';
      data['id'] = id
      data['wall_id'] = $('.menu-item-wall.active-menu-wl').data('wall');
      var block = $('.column-panel-list.word');

      if(!data['wall_id']){
        toastr.info('Create a first wall')
        return false
      }
      
      Column.create(data).then(ColumnAddSuccessFn, ColumnAddErrorFn);
        //toastr.error('sdsd34234242342hd');

        function ColumnAddSuccessFn(data, status, headers, config) {
            console.log('addColumnFromList', data);

            block.html(' ');
            $('.menu-item.menu-item-add').click();


            $scope.columns.unshift(data.data);

             
            for(var i = 0; i < $scope.columns.length; i++){
              var s = i + 1;
              $scope.columns[i].sort = s;
            }

            $timeout(function () {
              console.log($scope.columns, $('.header-column-sort-li.left').first());
              $('.header-column-sort-li.left').first().click();
            }, 500);

        }
        function ColumnAddErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        }

    };

    $scope.sendFormAddColumn = function($event, type){
      console.log('form');
      var t = $($event.currentTarget);
      var parent = $(t).parents('.cc-add-column');
      var column_parent = $(t).parents('.column.column-add-column');
      var data = {};
      var name = parent.find('.input-add-column').val();
      if(name.length==0){
        toastr.warning('Not name');
        return false
      }
      else if(name.length>25){
        toastr.warning('Max length');
        return false
      }
      

      console.log('n', name);
      var wall_id = $('.menu-item-wall.active-menu-wl').data('wall');
      data['wall_id'] = wall_id;
      if(type==1){
        data['types'] = 'site';
        data['link'] = name;
        data['wall_id'] = wall_id;
        //$scope.addColumn();
        //Column.create(link, 'site', wall_id).then(ColumnAddSuccessFn, ColumnAddErrorFn);
        Column.create(data).then(ColumnAddSuccessFn, ColumnAddErrorFn);
        //toastr.error('sdsd34234242342hd');

        function ColumnAddSuccessFn(data, status, headers, config) {
        console.log('create', data);
        //toastr.error('gghdfhd');
        var block = $('.column-panel-list.word');
        if(data.data.failure){
          toastr.error(data.data.failure);
        }
        else{
            
            console.log(data.data.data.length);
            var one = data.data.data[0];
            block.find('.column-content-ul-list-sub-menu').html(' ');
            block.find('.column-header-title-list').html('<b>' + one.short_url + '</b> Feeds');
            block.find('.column-header-list').css({'background': one.color, 'border-color': one.color, 'max-height': '35px'});
            block.find('.column-header-img-list').css({'border-color': one.color});
            block.find('.column-header-img-list > img ').attr('src', one.favicon);
            block.find('.column-content-list-sub-menu').css({'border-color': one.color});
            for (var i = 0; i < data.data.data.length; i++) {
                var e =  data.data.data[i];
                var t = '<li class="list-group-item"><span style="background:'+one.color+';" class="media-list-body-dots"></span><div class="media-list"><div class="media-list-body"><h4 class="media-list-heading">'+e.name+'</h4><h4 class="media-list-heading-small">'+e.rss+'</h4></div><div  class="media-list-right"><div class="mini-column-add" data-id="'+e.id+'" style="color: '+one.color+';border-color: '+one.color+';">ADD</div></div></div></li>';
                block.find('.column-content-ul-list-sub-menu').append(t);
            }
            block.addClass('active');
            $('.mini-column-add').click(function(){
              $scope.addColumnFromList($(this).data('id'))
            });

            column_parent.addClass('column-add-frommini-active');

        }
        //console.log($scope.columns.unshift(data.data.result));

        }


        function ColumnAddErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        //Snackbar.error(data.error);
         }

        /*$timeout(function(){
                    $scope.showMenuColumn(10);
                }, 1000);*/


      }
      else if(type==2){
        if(parent.find('.add-source-group-block-div.source > .add-source-group-block-img.active').length==0){
          toastr.warning('Not source');
          return false
        }
        /*else if(parent.find('.add-source-group-block-wall.active').length==0){
          toastr.warning('Not wall');
          return false
        }*/
        else{
          var sources = [];
          var matchings = [];
          var excludings = [];
          var walls = [];
          parent.find('.add-source-group-block-div.source > .add-source-group-block-img.active').each(function(indx, element){
             sources.push($(element).data('id'));
          });
          parent.find('.add-source-group-block.matching > .add-source-group-block-word > span').each(function(indx, element){
             matchings.push($(element).html());
          });
          parent.find('.add-source-group-block.excluding > .add-source-group-block-word > span').each(function(indx, element){
             excludings.push($(element).html());
          });
          parent.find('.add-source-group-block-wall.active').each(function(indx, element){
             walls.push($(element).data('id'));
          });
          
          data['name'] = name;
          data['wall_id'] = wall_id;
          data['icon'] = ' ';
          data['color'] = parent.find('.menu-item-img-word-btn').data('color');
          data['sources'] = sources;
          data['matchings'] = matchings;
          data['excludings'] = excludings;
          data['walls'] = walls;
          data['size'] = parseInt(parent.find('.acs-show-group-span.acs-size.acs-size-active').data('size'));
          data['types'] = 'collection';
          data['link'] = 'collection';

          console.log("DATA", data);
          Column.create(data).then(ColumnAddSuccessFn2, ColumnAddErrorFn2);
          function ColumnAddSuccessFn2(data, status, headers, config) {
            console.log('create', data); 
            if(data.data.success){
              $('.menu-item.menu-item-add-column').click();
              $scope.columns.unshift(data.data);
              console.log($scope.columns);

              for(var i = 0; i < $scope.columns.length; i++){
                if(i != 0){
                  $scope.columns.sort = $scope.columns.sort + 1;
                  //$scope.columns.st.sort = $scope.columns.sort + 1;
                }
              }

              $timeout(function () {
                $('.header-column-sort-li.left').first().click();
              }, 500);

            }
          }
          function ColumnAddErrorFn2(data, status, headers, config) {
          console.log(data, status, headers, config);
          }

        }

        console.log('2', data);


      }
    }

    $scope.colorAddWall = function($event){
      var e = $($event.currentTarget),
          color = e.data('color'),
          prt = e.parents('.add-wall-group');
      prt.find('.add-wall-form-icon').css({'background-color': color});
      prt.find('.add-wall-color').removeClass('active');
      e.addClass('active');
    }

    $scope.sendAddWall = function($event){
      var e = $($event.currentTarget);
      console.log('sendAddWall', e);
      var prt = e.parents('.form-group-add-wall');
      var name = prt.find('.input-add-column').val();
      var color = prt.find('.add-wall-color.active').data('color');

      console.log('sendAddWall', name, color);
      if(!name){
        toastr.error('No name');
        return false
      }

        //$scope.addColumn();
      Wall.create(name, color).then(WallAddSuccessFn, WallAddErrorFn);
        //toastr.error('sdsd34234242342hd');

      function WallAddSuccessFn(data, status, headers, config) {
        console.log('create wall', data);
        if(data.data.failure){
          toastr.error(data.data.failure);
        }
        else{
          $scope.addGroup(); 

          for(var i = 0; i < $scope.walls.length; i++){
            $scope.walls[i].current = false;
          }
          $scope.walls.unshift(data.data.data);
          $scope.columns = [];

          $location.path("/@"+$scope.slugusername+"/"+ data.data.data.name)
        }
      }


      function WallAddErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
      }
    }


    /******* settings *******/

    $scope.settingsEditSortColumn = function($event, action){
      var element = $($event.currentTarget);
      var column_id = $(element).parents('.column').data('column');
      var column_id_prev = $(element).parents('.column').prev().data('column');
      var column_id_next = $(element).parents('.column').next().data('column');
      console.log();
      var clmn = null,
          clmn_prev = null,
          clmn_next = null;
      var sorter = [];
      var clmn_length = $scope.columns.length - 1;

      for(var i = 0; i < $scope.columns.length; i++){
        if( ($scope.columns[i].id == column_id) && ($scope.columns[i].types == 1 || $scope.columns[i].types == 2) ){
          clmn = $scope.columns[i];
        }
        if( ($scope.columns[i].id == column_id_prev) && ($scope.columns[i].types == 1 || $scope.columns[i].types == 2) ){
          clmn_prev = $scope.columns[i];
        }
        if( ($scope.columns[i].id == column_id_next) && ($scope.columns[i].types == 1 || $scope.columns[i].types == 2) ){
          clmn_next = $scope.columns[i];
        }

      }

      if(element.hasClass('left')){
        if(clmn.sort > 1){
          clmn_prev.sort = clmn.sort;
          clmn_prev.st.sort = clmn.sort;
          clmn.sort -= 1;
          clmn.st.sort -= 1;
        }
      }
      else if(element.hasClass('right')){
        if(clmn.sort <= clmn_length){
          clmn_next.sort = clmn.sort;
          clmn_next.st.sort = clmn.sort;
          clmn.sort += 1;
          clmn.st.sort += 1;
        }
      }

      for(var i = 0; i < $scope.columns.length; i++){
        $scope.columns[i];
        sorter.push({'column': $scope.columns[i].id, 'sort': $scope.columns[i].sort, 'wall_id': $scope.columns[i].wall_id})
      }

      console.log('settingsEditSortColumn', sorter);
      Column.settings_sort(sorter).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('settings_sort', data);

      }

      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };

    $scope.settingsEditRemoveColumn = function($event){
      var element = $($event.currentTarget);
      var column_id = $(element).parents('.column').data('column');
      console.log('settingsEditRemoveColumn', element);

      for(var i = 0; i < $scope.columns.length; i++){
        if( ($scope.columns[i].id == column_id) && ($scope.columns[i].types == 1 || $scope.columns[i].types == 2) ){
          //clmn = $scope.columns[i];
          $scope.columns.splice(i,1)
        }
      }

      

      
      Column.settings_delete(column_id).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
          console.log('column_id', data);

      }

      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }


    };
    

    $scope.settingsEditSizeColumn = function($event, el, em){
      console.log('SettingsEditSizeColumn', $event, el, em);
      var element = $event.currentTarget,
          pt = $(element).parents('.column'),
          id = pt.data('column');
      
      if(pt.hasClass('column-add-column')){
        pt.find('.acs-show-group-span.acs-size').removeClass('acs-size-active');
        $(element).addClass('acs-size-active');  
      }
      else{
        pt.find('.acs-show-group-span.acs-size').removeClass('acs-size-active');
        $(element).addClass('acs-size-active');
        pt.removeClass().addClass('column ng-scope active-clm size'+em);
        Column.settings_size(el, em).then(ColumnSuccessFn, ColumnErrorFn);

        function ColumnSuccessFn(data, status, headers, config) {
          console.log('settings size', data);
          if(data.data.success){
              for(var i = 0; i < $scope.columns.length; i++){
              if($scope.columns[i].id == id){
                $scope.columns[i].st.size = em;
              }
            } 
          }

        }

        function ColumnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }
      }

    };

    $scope.settingsEditRealtimeColumn = function($event, el){
      console.log('settingsEditRealtimeColumn', $event, el);
      var element = $($event.currentTarget);
      var pt = element.parents('.column');
      var id = pt.data('column');

      var em = 'No';
      var es = false;
      if(element.hasClass('checked-site')){
        em = 'Yes';
        es = true;
        element.removeClass('checked-site').addClass('checked-word');
      }
      else{
        element.removeClass('checked-word').addClass('checked-site');
      }
      
      
      Column.settings_realtime(el, em).then(ColumnSuccessFn, ColumnErrorFn);

      function ColumnSuccessFn(data, status, headers, config) {
          console.log('settings_realtime', data);
          if(data.data.success){
              for(var i = 0; i < $scope.columns.length; i++){
              if($scope.columns[i].id == id){
                $scope.columns[i].st.realtime = es;
                console.log('settings_realtime i ', $scope.columns[i]);
              }
            } 
          }

      }

      function ColumnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
      }
      
    };


    $scope.settingsEditNotiColumn = function($event){
      var e = $($event.currentTarget);
      
      if(e.hasClass('checked-site')){
        e.removeClass('checked-site').addClass('checked-word');
      }
      else{
        e.removeClass('checked-word').addClass('checked-site');
      }
    };

    $scope.settingsEditPlusWordColumn = function($event, el){
      console.log('settingsEditPlusWordColumn', $event, el);
      var element = $event.currentTarget;
      $(element).addClass('active').removeClass('icon-plus-squared').addClass('icon-cancel');
      var pos = $(element).offset();
      var parent = $(element).parents('.column-content-sub-menu');

      
      /*var lf = pos.left + 35;
      $('.column-st-word').remove();
      var html = '<div style="top:'+pos.top+'px; left:'+ lf +'px" class="popup column-st-word"><div class="tooltip-settings red"><input id="inp"><span class="button-save">save</span></div></div>';
      $('body').append($compile(html)($scope));
      $('#inp').keyup(function(){
        console.log('this', $(this).val());
        var value = $(this).val();
        var block = parent.find('.ac-form-acs-filter-sort > .acs-show-group-div');
        if(value.length==1){
          block.append('<span class="acs-show-group-span acs-new acs-load"><span>'+value+'</span> </span>');
        }
        else{
          block.find('.acs-show-group-span.acs-new.acs-load:last-child').html(value)
        }
      });*/
      //$scope.$apply();
      //$('body').append('<div style="top:'+pos.top+'px; left:'+ lf +'px" class="popup column-st-word"><div class="tooltip-settings red"><input ng-click="console.log(\'yes\')"></div></div>')

    };

   

    $scope.settingsEditSourceWColumn = function($event, el){
      console.log('settingsEditSourceWColumn', $event, el);
      var tooltip = null;
      Column.get_settings_w_source().then(ColumnSuccessFn, ColumnErrorFn);
        //toastr.error('sdsd34234242342hd');

        function ColumnSuccessFn(data, status, headers, config) {
        console.log('get_settings_w_source', data);
        tooltip = data.data;
        /*//toastr.error('gghdfhd');
        if(data.data.failure){
          toastr.error(data.data.failure);
        }
        //console.log($scope.columns.unshift(data.data.result));*/




      var element = $event.currentTarget;
      var pos = $(element).offset();
      var lf = pos.left + 35;
      $('.column-st-source').remove();

      var html = '<div style="top:'+pos.top+'px; left:'+ lf +'px" class="popup column-st-source">'+ tooltip +'</div>';
      $('body').append($compile(html)($scope));
    }
      function ColumnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
        toastr.error(data.error);
       }
      //$scope.$apply();
      //$('body').append('<div style="top:'+pos.top+'px; left:'+ lf +'px" class="popup column-st-word"><div class="tooltip-settings red"><input ng-click="console.log(\'yes\')"></div></div>')

    };

    /*$('div').not('.popup').click(function(){
        console.log('this vcbvb');
        if($('.popup').length>0){ $('.popup').remove() }
    });*/
     
    $scope.openRelatedColumn = function($event){
      /*var element = $($event.currentTarget);
      var c = $(element).parents('.column').data('column');
      var header = $(element).parents('.column-header');
      var column = $(element).parents('.column');
      var size = parseInt($(element).parents('.column').getClassRegex(/^size/).replace(/\D+/g,""));
      var next = false;
      var id = null;
      
      element.toggleClass('open');
      

      function handler(id){
        Column.get_related(c).then(ReLatedGetSuccessFn, ReLatedGetErrorFn);

        function ReLatedGetSuccessFn(data, status, headers, config) {
          console.log('ReLatedGetSuccessFn', data.data);
          if(data.data.success){
              for(var i = 0; i < $scope.columns.length; i++){
              if($scope.columns[i].id == id){
                console.log('$scope.columns[i].related_to_items', $scope.columns[i].related_to_items);
                $scope.columns[i].related_to_items = data.data.related_to_items;
                $scope.columns[i].items = data.data.related_to_items;
              }
            } 
          }
        }
        function ReLatedGetErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
        }

      }



      if(element.hasClass('open')){
        console.log('open');
        header.addClass('active');
 
        var r =  {"id": 0, "related_column": true, "is_active": false, "name": 'Related for this column', "icon": 'http://contter.dev/images/icon-re.png', "sort": 1, "items": [],
               "types": 4, "st": {'size': size, 'sort': 5, 'type': 4},
               "column_to_rel_column": null, "related_to_items": []}

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == c){
            console.log($scope.columns[i]);
            r["column_to_rel_column"] = $scope.columns[i].column_to_rel_column;
            r["related_to_items"] = $scope.columns[i].related_to_items;
            if($scope.columns[i].column_to_rel_column){next = true;r["id"] = $scope.columns[i].column_to_rel_column;id = $scope.columns[i].column_to_rel_column;};
            $scope.columns.splice(i+1, 0, r);
            //$scope.$apply();
          }
        }

        if(next){
          handler(id); 
        }

      }
      else{
        $timeout(function () {
          console.log('column', column.next().find('.related_post_close').click());
          header.removeClass('active');
        })
        //$('.column.rel-column .related_post_close').click();
      }*/

    }

    $scope.closePostRelatedColumn = function($event){
      v/*ar e = $event.currentTarget;
      var c = $(e).parents('.column').data('column');
      var prev_c = $(e).parents('.column').prev();
      
      for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == c){
            $scope.columns.splice(i,1)
          }
      }
      if($(e).parents('.column').hasClass('related-column')){
        if(c==0){
          $('.column-header').removeClass('active').find('.column-header-to-relevat').removeClass('open')
        }
        else{
          prev_c.find('.column-header').removeClass('active');
          prev_c.find('.column-header-to-relevat').removeClass('open');
        }
      }
      if($(e).parents('.column').hasClass('rel-column')){
        $('.post__to__relevat').removeClass('open')
      } 
      //*/

    }

    $scope.Related = function($event, el){
      var e = $event.currentTarget;
      var element = $(e);
      var c = $(e).parents('.column').data('column');//phone.replace(/\D+/g,"")removeClassRegex(/^size/)
      console.log('Related', c, el);
      var clmn = null;

      for(var i = 0; i < $scope.columns.length; i++){
        if($scope.columns[i].id == c){
          clmn = $scope.columns[i]
        }
      }

      clmn.launch = 'related';

      Column.get_related(c, el).then(fnSuccessFn, fnErrorFn);
          //toastr.error('sdsd34234242342hd');

      function fnSuccessFn(data, status, headers, config) {
        console.log('get_related', data.data.data);
        if(data.data.data){
          clmn.related_to_items = data.data.data.items.slice(0,9)
        }
      }
      function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
      }

     /*
      element.toggleClass('open');

      if(element.hasClass('open')){

        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == 0){//if($scope.columns[i].types == 4){
            toastr.error('Already open a similar column, close it.');
            return false
          }
        }


        var r =  {"id": 0, "is_active": false, "name": 'Related for this link', "icon": 'http://contter.dev/images/icon-re.png', "sort": 1, "items": [],
               "types": 4, "st": {'size': size, 'sort': 5, 'type': 4}}
        for(var i = 0; i < $scope.columns.length; i++){
          if($scope.columns[i].id == c){
            console.log($scope.columns[i]);
            $scope.columns.splice(i+1, 0, r);
            $scope.$apply();
          }
        } 

        Column.related_create(c, el, size).then(ReLatedCreateSuccessFn, ReLatedCreateErrorFn);
          //toastr.error('sdsd34234242342hd');

        function ReLatedCreateSuccessFn(data, status, headers, config) {
         console.log('related_create', data.data.data);
          if(data.data.data){
            for(var i = 0; i < $scope.columns.length; i++){
             if($scope.columns[i].id == 0){
               $scope.columns[i] = data.data.data
              }
           } 
          }
        }
        function ReLatedCreateErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
        }
      }
      else{
        var next = $(e).parents('.column').next();
        if(next.hasClass('rel-column')){
          console.log("$(e).parents('.column')", $(e).parents('.column').next())
          for(var i = 0; i < $scope.columns.length; i++){
            if($scope.columns[i].id == next.data('column')){
              $scope.columns.splice(i,1);
              $scope.$apply();
            }
          }
        }
      }*/

    };  

    $scope.CloseRelated = function($event){
      var e = $event.currentTarget;
      var element = $(e);
      var c = $(e).parents('.column').data('column');
      console.log('CloseRelated', c);
      var clmn = null;

      for(var i = 0; i < $scope.columns.length; i++){
        if($scope.columns[i].id == c){
          clmn = $scope.columns[i]
        }
      }

      clmn.launch = 'standart';

    };  

    /****** end settings *****/

    //create
    $scope.ReContent = function($event, el){
      var e = $event.currentTarget;
      var element = $(e);
      var c = element.parents('.column').data('column');
      var action = true;
      console.log('ReContent', c, el);

      element.toggleClass('active');

      if(!element.hasClass('active')){
        action = false
      }
      
      //if(element.hasClass('active')){
        Column.recontent(c, el, action).then(recontentSuccessFn, recontentErrorFn);

      function recontentSuccessFn(data, status, headers, config) {
        console.log('recontentSuccessFn', data.data);
        if(data.data.success){
          for(var i = 0; i < $scope.columns.length; i++){
            if($scope.columns[i].id == c){
              for(var s = 0; s < $scope.columns[i].items.length; s++){
                if($scope.columns[i].items[s].id == el){
                  if(data.data.action == 'add'){
                    $scope.columns[i].items[s].if_recontent = action;
                    $scope.columns[i].items[s].if_recontent_id = data.data.column_id;
                  }
                  else{
                    $scope.columns[i].items[s].if_recontent = action;
                    $scope.columns[i].items[s].if_recontent_id = null;
                  }
                }
              }
            }
          } 
        } 
      }
      function recontentErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
      //}
      //else{
      //  console.log('no active')
      //}
      
    }; 

    //create FAVOURITE
    $scope.ToFavoutite = function($event, el){
      var e = $event.currentTarget;
      var element = $(e);
      var c = element.parents('.column').data('column');
      var action = 'add';
      var act = true;
      console.log('ToFavoutite', c, el);

      element.toggleClass('active');
      
      if(!element.hasClass('active')){
        action = 'remove';
        act = false;
      }

      Column.favourite(c, el, action).then(fnSuccessFn, fnErrorFn);

      function fnSuccessFn(data, status, headers, config) {
        console.log('ToFavoutite', data.data);
        if(data.data.success){
          for(var i = 0; i < $scope.columns.length; i++){
            if($scope.columns[i].id == c){
              for(var s = 0; s < $scope.columns[i].items.length; s++){
                if($scope.columns[i].items[s].id == el){
                  $scope.columns[i].items[s].if_favourite = act;
                }
              }
              if($scope.columns[i].launch == 'favourite' && action == 'remove'){
                for(var s = 0; s < $scope.columns[i].favourites_items.length; s++){
                  if($scope.columns[i].favourites_items[s].id == el){
                    $scope.columns[i].favourites_items.splice(s,1)
                  }
                }
              }
            }
          } 
        }
      }
      function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }
      
      
    }; 

    /* preloader */
    $scope.columnPreloader = function(element, column, action){
      var clmn = null,
          ids = [],
          prt = $(element).parents('.column'),
          r = $(prt).find('.column-panel.column-rel-panel'),
          st = $(prt).find('.column-panel.column-standart-panel'),
          st_page = st.data('page'),
          noti_st = st.find('.column-notification'),
          noti_r = r.find('.column-notification');
      
      for(var i = 0; i < $scope.columns.length; i++){
        if($scope.columns[i].id == column){
          clmn = $scope.columns[i]
        }
      }

      for(var i = 0; i < clmn.items.length; i++){
        ids.push(clmn.items[i].id)
      }

      if(action=='standart' && noti_st.hasClass('active')){
        for(var i = 0; i < $scope.ws_new_links.length; i++){
          console.log($scope.ws_new_links[i]);
          ids.push($scope.ws_new_links[i].post.id)
        }
      }

      console.log('ids', ids);

      if(action=='standart'){
        console.log('columnPreloader', st_page);

        Column.preloader(column, st_page, ids, action, $scope.userType).then(preloaderSuccessFn, preloaderErrorFn);

        function preloaderSuccessFn(data, status, headers, config) {
          console.log('preloaderSuccessFn', $(element), data.data);
          if(data.data.success){

            for(var i = 0; i < data.data.data.items.length; i++){
              clmn.items.push(data.data.data.items[i]);
            }
            
            console.log('preloaderSuccessFn', clmn);

            $(element).find('.pre-loader-after-links').remove();
            $(element).find('.column-content').removeClass('pre-loader');
            st.data('page', data.data.data.page)
          }
        }
        function preloaderErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          $(element).find('.pre-loader-after-links').remove();
          $(element).find('.column-content').removeClass('pre-loader');
          st.data('page', data.data.data.page)
        }

    }

    }
    /* end preloader */

    /* preloader Anonymus */
    $scope.columnPreloaderAnonymus = function(element, column, action){
      console.log('columnPreloaderAnonymus', element, column, action);
      var clmn = null,
          ids = [],
          prt = $(element).parents('.column'),
          r = $(prt).find('.column-panel.column-rel-panel'),
          st = $(prt).find('.column-panel.column-standart-panel'),
          st_page = st.data('page'),
          noti_st = st.find('.column-notification'),
          noti_r = r.find('.column-notification');
      

      $(element).find('.pre-loader-after-links').remove();
      $(element).find('.column-content').removeClass('pre-loader');
      st.data('page', 1)
    }
    /* end preloader Anonymus */

    /* preloader Alien */
    $scope.columnPreloaderAlien = function(element, column, action){
      console.log('columnPreloaderAlien', element, column, action);
      var clmn = null,
          ids = [],
          prt = $(element).parents('.column'),
          r = $(prt).find('.column-panel.column-rel-panel'),
          st = $(prt).find('.column-panel.column-standart-panel'),
          st_page = st.data('page'),
          noti_st = st.find('.column-notification'),
          noti_r = r.find('.column-notification');
      

      $(element).find('.pre-loader-after-links').remove();
      $(element).find('.column-content').removeClass('pre-loader');
      st.data('page', 1);

      prt.find('.cntr-pallet-mini').click();

      $('.tooltip-add-column').remove();


      var e = prt.find('.cntr-pallet-add');
      var tooltip_content = '<p>Add to my column!</p><p>and read all the news!</p>'
      var tooltip = $('<div id="tooltips" class="tooltip-add-column "><div class="arrow"></div><div class="tooltip-content">'+tooltip_content+'</div></div>');
      

      $body.append(tooltip);
      $timeout(function () {

        tooltip.css({'top': (e.offset().top + 45), 'left': (e.offset().left - 22)});
        $timeout(function () {
          tooltip.addClass('show animate-add-column-tooltip')
        }, 1000);

      }, 450);

      $timeout(function () {
          tooltip.remove();
      }, 10000);


    }
    /* end preloader Anonymus */

    $scope.getReContent = function(column){
      var clmn = null;
      console.log('getReContent', column);

      var $column = $('.column[data-column="'+column+'"]');

      for(var i = 0; i < $scope.columns.length; i++){
            if($scope.columns[i].id == column){
              clmn = $scope.columns[i]
              //$scope.columns[i].get_recontent = true
            }
      }

      clmn.launch = 'recontent';
        
      Column.get_recontent(column).then(GetReContentSuccessFn, GetReContentErrorFn);
        //toastr.error('sdsd34234242342hd');

      function GetReContentSuccessFn(data, status, headers, config) {
        console.log('get_recontent', data.data);
        if(data.data.failure == 'none'){
          console.log('none');
          clmn.get_recontent = true
          
        }
        else if(data.data.success == 'data'){
          clmn.get_recontent = true;
          clmn.if_recontent = true;
          clmn.recontent_items = data.data.recontent_items;
        }
        
        
      }
        function GetReContentErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }



    };  

    /**** get favoureite column ****/
    $scope.getFavourites = function(column){
      var clmn = null;
      console.log('getFavourites', column);

      var $column = $('.column[data-column="'+column+'"]');

      for(var i = 0; i < $scope.columns.length; i++){
            if($scope.columns[i].id == column){
              clmn = $scope.columns[i]
              //$scope.columns[i].get_recontent = true
            }
      }

      clmn.launch = 'favourite';

      /*$timeout(function () {
        clmn.get_favourites = true;
        clmn.if_favourites = true;
        clmn.favourites_items = [];
      }, 3500);*/
        
      Column.get_favourite(column).then(fnSuccessFn, fnErrorFn);
        //toastr.error('sdsd34234242342hd');

      function fnSuccessFn(data, status, headers, config) {
        console.log('get_favourite', data.data);
        if(data.data.failure == 'none'){
          console.log('none');
          clmn.get_favourites = true
          
        }
        else if(data.data.success == 'data'){
          clmn.get_favourites = true;
          clmn.if_favourites = true;
          clmn.favourites_items = data.data.favourites_items;
        }
        
        
      }
        function fnErrorFn(data, status, headers, config) {
        console.log(data, status, headers, config);
      }

    };   
    /**** end favourites column ****/

    /**** start profile ****/
    $scope.ShowProfile = function($event){
      console.log('ShowProfile', $scope.user.displayName);
      var body = $('body');

      //if(!$scope.account){
        
      //}

      body.toggleClass('open-profile').toggleClass('open-profile-animate');

      function fh(){
        $timeout(function () {
          $('.profileavatarimg').avatarLoad();
        }, 100);
      };

      if(body.hasClass('open-profile')){
        $scope.getUser($scope.user.displayName, fh)
      }
      else{
        $('.tooltip-user-card').remove();
        $('.user__card__settings').removeClass('active');

      }


      $timeout(function () {
        body.removeClass('open-profile-animate')
      }, 1500);


      //$location.path('/@'+ $scope.user.displayName)

      /*var r = {'icon': 'http://contter.dev/images/icon-profile.png', 'id': 0, 'items': [], 'name': 'andrey', 'sort': 0, 'st': [], 'types': 'profile'};
      $scope.profiles.unshift(r);*/
      //$scope.$apply();
    };  

    $scope.TypeUserSettings = function($event){
      console.log('TypeUserSettings', $event);
      var element = $($event.currentTarget);
      var em = 'EVERYONE';
      var es = false;
      if(element.hasClass('checked-site')){
        em = 'ME';
        es = true;
        element.removeClass('checked-site').addClass('checked-word');
      }
      else{
        element.removeClass('checked-word').addClass('checked-site');
      }

      Account.settingsTypesProfile(em)
      .then(function(response) {
        console.log('settingsTypesProfile', response)
      })
      .catch(function(response) {
        toastr.error(response.data.message, response.status);
      });

    };  

    $scope.PictureUserSettings = function($event){
      var src = URL.createObjectURL($event.target.files[0]);
      var user_card = $('.user__card__block');
      var image = $event.target.files[0];
      console.log('settingsPictureProfile image', image.size);

      user_card.css({'background-image': 'url('+src+')'});

      Account.settingsPictureProfile(image)
      .then(function(response) {
        console.log('settingsPictureProfile', response)
      })
      .catch(function(response) {
        toastr.error(response.data.message, response.status);
      });
     /* $http({
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: {
                upload: $event.target.files[0]
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        })
        .success(function (data) {
console.log('settingsPictureProfile', data)
        })
        .error(function (data, status) {

        }); */


      $('.tooltip-user-card.tooltip-user-settings').remove();
      $('.user__card__settings.user__card__settings__set').removeClass('active');
    };  

    $scope.ColorUserSettings = function($event){
      var e = $($event.currentTarget);
      var user_card = $('.user__card__block');
      var color = e.data('color');

      $scope.user.color = color;

      Account.settingsColorProfile(color)
      .then(function(response) {
        console.log('settingsColorProfile', response)
      })
      .catch(function(response) {
        toastr.error(response.data.message, response.status);
      });

      user_card.removeClassRegex(/^uc_color_/);
      user_card.addClass('uc_color_' + color);
    }; 

    $scope.ShowUserSettings = function($event){
      console.log('ShowUserSettings', $event);
      var e = $($event.currentTarget);
      //var loader = $('<div class="loader-tooltip active"><div class="loader  loader-ellipsis-tooltip" data-type="default"></div></div>');
      var type_user = 'checked-site';
      if($scope.user.types == 2){
        type_user = 'checked-word';
      }


      var type_profile = '<div class="user__card_type_profile ac-form-acs-public"><h3>Who can see my profile </h3><div class="acs_toggle_block '+type_user+'"><div class="acs_toggle_button"><div class="acs_toggle_site_text">EVERYONE</div><div class="acs_toggle_word_text">ONLY ME</div><span class="acs_toggle_handle"><span></span></span></div></div></div>';

      var color_html = '<ul class="user__card_color-ul "> <li style="background-color:#f44336" data-color="f44336" class="user__card_color"></li><li style="background-color:#e91e63" data-color="e91e63" class="user__card_color"></li><li style="background-color:#9c27b0" data-color="9c27b0" class="user__card_color"></li><li style="background-color:#673ab7" data-color="673ab7" class="user__card_color"></li><li style="background-color:#2196f3" data-color="2196f3" class="user__card_color"></li><li style="background-color:#03a9f4" data-color="03a9f4" class="user__card_color"></li><li style="background-color:#00bcd4" data-color="00bcd4" class="user__card_color"></li><li style="background-color:#009688" data-color="009688" class="user__card_color"></li><li style="background-color:#4caf50" data-color="4caf50" class="user__card_color"></li><li style="background-color:#8bc34a" data-color="8bc34a" class="user__card_color"></li><li style="background-color:#cddc39" data-color="cddc39" class="user__card_color"></li><li style="background-color:#ffc107" data-color="ffc107" class="user__card_color"></li><li style="background-color:#ff9800" data-color="ff9800" class="user__card_color"></li><li style="background-color:#ff5722" data-color="ff5722" class="user__card_color"></li><li style="background-color:#795548" data-color="795548" class="user__card_color"></li><li style="background-color:#9e9e9e" data-color="9e9e9e" class="user__card_color"></li><li style="background-color:#607d8b" data-color="607d8b" class="user__card_color"></li></ul>';
      var color_profile = '<div class="user__card_color_profile" ><h3>COLOR </h3> ' + color_html + ' </div>';

      var picture_html = '<div class="picture_profile_block"> <input id="usercardpicture" name="usercardpicture" type="file" class="picture_profile_img" accept="image/jpeg" /> <span class="picture_profile_icon"></span><p class="picture_profile_p">UPLOAD PHOTO</p></div>';
      var picture_profile = '<div class="user__card_picture_profile" ><h3>Profile Picture </h3> ' + picture_html + ' </div>';

      var content = '<div class="user_card_set_block" > ' + type_profile + ' ' + color_profile + ' ' + picture_profile + ' </div>';

      var tooltip = $('<div id="tooltips" class="tooltip-user-card tooltip-user-settings show"><div class="arrow"></div><div class="tooltip-content">'+content+'</div></div>');
      e.toggleClass('active');
      if($('.tooltip-user-card').length<=0){
        //tooltip.append(loader);
        $compile(tooltip);
        $('body').append(tooltip);

        tooltip.find('.acs_toggle_block').click(function(event){
          $scope.TypeUserSettings(event);
        });

        tooltip.find('.user__card_color').click(function(event){
          $scope.ColorUserSettings(event);
        });

        tooltip.find('#usercardpicture').change(function(event){
          $scope.PictureUserSettings(event);
        });

        

        console.log('ShowUserSettings', e.position(), e.offset(), tooltip);
        tooltip.css({'top': e.offset().top + 45, 'left': (e.offset().left + 2)});
        $timeout(function () {
          tooltip.addClass('show')
        }, 1000);
      }
      else{
        $('.tooltip-user-card').remove();
      }
    };

    $scope.ShowUserCardInfo = function($event){
      console.log('ShowUserCardInfo', $scope.account);
      var e = $($event.currentTarget);
      var content = '<div class="user_card_infc_block">' +
          '<div class="user_card_infc_item"><p>FOLLOWERS</p><span>'+$scope.account.followers_count+'</span></div>' +
          '<div class="user_card_infc_item"><p>FOLLOWING</p><span>'+$scope.account.following_count+'</span></div>' +
          '<div class="user_card_infc_item"><p>COLUMNS</p><span>'+$scope.account.columns_count+'</span></div>' +
          '<div class="user_card_infc_item"><p>WALLS</p><span>'+$scope.account.walls_count+'</span></div>' +
          '</div>';
      var tooltip = $('<div id="tooltips" class="tooltip-user-card tooltip-user-infocard show"><div class="arrow"></div><div class="tooltip-content">'+content+'</div></div>');
      
      e.toggleClass('active');

      if($('.tooltip-user-card.tooltip-user-infocard').length<=0){
        $('.tooltip-user-card').remove();
        $compile(tooltip);
        $('body').append(tooltip);

        

        console.log('ShowUserSettings', e.position(), e.offset(), tooltip);
        tooltip.css({'top': (e.offset().top - tooltip.height() - 5), 'left': (e.offset().left - 5)});
        $timeout(function () {
          tooltip.addClass('show')
        }, 1000);
      }
      else{
        $('.tooltip-user-card').remove();
      }
    };

    $scope.ShowUserMyRecontent = function($event){
      console.log("ShowUserMyRecontent", $event);
      var el = $($event.currentTarget),
          body = $('body'),
          rec = $scope.recontents,
          next = false;

      if(!body.hasClass('open-recontent') && !rec.active){
        body.addClass('open-recontent');
      }
      else if(body.hasClass('open-recontent') && rec.active_my){
        body.removeClass('open-recontent');
        $scope.recontents.proccess_my = false;
        $scope.recontents.active_my = false;
        return false
      }

      $scope.recontents.active = false;
      $scope.recontents.active_my = true;
      $scope.recontents.proccess = false;
      
      if(!$scope.recontents.proccess_my){
        
        $scope.recontents.proccess_my = true;
        $scope.recontents.my = true;
        $scope.recontents.if_check_my = false;

        if($scope.recontents.my_items.length>0){
          $scope.recontents.items = $scope.recontents.my_items;
        }
        else{
          $scope.recontents.items = [];
        }

        Column.get_my_recontent().then(fnSuccessFn, fnErrorFn);
  
        function fnSuccessFn(data, status, headers, config) {
          console.log('get_my_recontent', data.data);
          $scope.recontents.my_items = data.data.recontent_items;
          $scope.recontents.items = data.data.recontent_items;
        }
          function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
        }
      }

    };

    $scope.ShowUserFeedRecontent = function($event){
      console.log("ShowUserFeedRecontent", $event);
      var el = $($event.currentTarget),
          body = $('body'),
          rec = $scope.recontents,
          next = false;

      console.log('$scope.recontents', $scope.recontents);

      if(!body.hasClass('open-recontent') && !rec.active_my){
        body.addClass('open-recontent');
      }
      else if(body.hasClass('open-recontent') && rec.active){
        body.removeClass('open-recontent');
        $scope.recontents.proccess = false;
        $scope.recontents.active = false;
        return false
      }
      
      $scope.recontents.active_my = false;
      $scope.recontents.active = true;
      $scope.recontents.proccess_my = false;
      
      if(!$scope.recontents.proccess){

        $scope.recontents.proccess = true;
        $scope.recontents.my = false;
        $scope.recontents.if_check = false;
        if($scope.recontents.feed_items.length>0){
          $scope.recontents.items = $scope.recontents.feed_items;
        }
        else{
          $scope.recontents.items = [];
        }
        
        //Column.get_feed_recontent().then(fnSuccessFn, fnErrorFn);
        Column.get_my_recontent().then(fnSuccessFn, fnErrorFn);
  
        function fnSuccessFn(data, status, headers, config) {
          console.log('get_feed_recontent', data.data);
          $scope.recontents.feed_items = data.data.recontent_items;
          $scope.recontents.items = data.data.recontent_items;
        }
          function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
        }
            }
    };    

    $scope.ShowUserNotification = function($event){
      var e = $($event.currentTarget);
      
      //var loader = $('<div class="loader-tooltip active"><div class="loader  loader-ellipsis-tooltip" data-type="default"></div></div>');
      var loader = $('<div class="loader-tooltip active"><div class="loader  loader-ellipsis-tooltip" data-type="default"></div></div>');
      

      /*var list = '';
      var i = 1;
      while (i <= 5){
        if(i==3){
          
          list += '<li class="user_card_noti_li user_card_noti_add_clmn"> <h1>TOM KRUZ </h1><p>created column</p> </li>';
        }
        else{
          list += '<li class="user_card_noti_li user_card_noti_follow"> <h1>licia vibess </h1><p>started following you</p> </li>';
        }
        
        i++
      }*/

      e.toggleClass('active');

      



      var content = 'n'//'<ul class="user_card_noti_list">'+list+'</ul>';
      

      var tooltip = $('<div id="tooltips" class="tooltip-user-card tooltip-user-notification show"><div class="arrow"></div><div class="tooltip-content"></div></div>');


      function handlerNoti(){
        var contents = [];
        var list = '';
        Account.getSettingsNotiProfile()
        .then(function(response) {
          console.log('getSettingsNotiProfile', response.data);
          contents = response.data.data;
          for(var i = 0; i < contents.length; i++){
          console.log('noti', contents[i])
            var username = contents[i].user.username;
            if(contents[i].user.first_name && contents[i].user.last_name){
              username = contents[i].user.first_name + ' ' + contents[i].user.last_name
            } 

            if(contents[i].types == 1){
              list += '<li class="user_card_noti_li user_card_noti_add_clmn"> <h1>'+username+' </h1><p>created column</p> </li>'
            }
            if(contents[i].types == 2){
              list += '<li class="user_card_noti_li user_card_noti_follow"> <h1>'+username+' </h1><p>started following you</p> </li>';
            }
          }

          if(contents.length == 0){
            list += '<li class="user_card_noti_li user_card_noti_nothing"> <h1>Nothing yet</h1> </li>'
          }

          var content = '<ul class="user_card_noti_list">'+list+'</ul>';
          tooltip.find('.loader-tooltip').remove();
          tooltip.find('.tooltip-content').html(content);
          tooltip.css({'top': e.offset().top + 41, 'left': e.offset().left});
        })
        .catch(function(response) {
          toastr.error(response.data.message, response.status);
        });
      }
      

      

      if($('.tooltip-user-card').length<=0){
        tooltip.append(loader);
        $('body').append(tooltip);

        handlerNoti();
        

        //tooltip.find('.tooltip-content').html(content);
      
        console.log('ShowUserNotification', e.position(), e.offset(), tooltip);
        tooltip.css({'top': ((e.offset().top - tooltip.height()) - 10 ), 'left': ((e.offset().left - tooltip.width()) + 30)});
        $timeout(function () {
          tooltip.addClass('show')
        }, 1000);
      }
      else{
        $('.tooltip-user-card').remove();
      }

    };  
    /**** end profile ****/

    //top wall
    $rootScope.$on('TopWallEmit', function(event, data) {
      $scope.topWall(data)
    });

    /*TOP WALL*/
    $scope.topWall = function(wall){
      console.log('topWall', wall);
      


      if(!$body.hasClass('open-top-wall')){
        $body.addClass('open-top-wall');
        $scope.top_column = true;
        Column.top(wall).then(fnSuccessFn, fnErrorFn);
  
        function fnSuccessFn(data, status, headers, config) {
          console.log('topWall', data.data, $scope.columns);
          //$scope.top_columns.items = data.data.data;
          for (var i = 0; i < data.data.data.length; i++) {
            console.log("data.data.data[i]", data.data.data[i])
            for (var g = 0; g < $scope.columns.length; g++) {
              if($scope.columns[g].id == data.data.data[i].column_id){
                $scope.columns[g].top_items = data.data.data[i].links
              }
            };
          };

        }
        function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
        }
      }
      else{
        $body.removeClass('open-top-wall');
        $scope.top_column = false;
        //$scope.$apply();
      }

    }

    $scope.addColumnFromUser = function($event, column_id){
      console.log('addColumnFromUser', column_id);
      var walls = [],
          e = $($event.currentTarget),
          column = e.parents('.column');

      $('.cntr-pallet-add').not(e).removeClass('active');

      e.toggleClass('active');

      if(!e.hasClass('active')){
        $('.tooltip-add-column').remove();
        return false
      }

      
      $('.tooltip-add-column').remove();
      var loader = '<div class="loader-tooltip active"><div class="loader  loader-ellipsis-tooltip" data-type="default"></div></div>';
      var tooltip = $('<div id="tooltips" class="tooltip-add-column "><div class="arrow"></div><div class="tooltip-content">'+loader+'</div></div>');
      var w = column.find('.cntr-pallet-full').width();

      column.append(tooltip);

      tooltip.css({'top': (e.offset().top + 45), 'left': 4, 'width': w});
      $timeout(function () {
          tooltip.addClass('show animate-add-column-tooltip')
      }, 200);


      Wall.list(column_id).then(fnSuccessFn, fnErrorFn);

        function fnSuccessFn(data, status, headers, config) {
          var d = data.data;
          walls = d.walls;
          console.log('saddColumnFromUser data', d);

          if(walls.length<=0){
            
            var tooltip_content = $('<div class="add-column-add-wall"><p>Nothing yet</p><p><a>Please create wall!<a></p></div>');
            handler(tooltip_content);//menu-item menu-item-add-wall
            tooltip_content.find('a').click(function(event){
              $('.menu-item.menu-item-add-wall').click();
              e.click()
            });

          }
          else{
            var list = ''
            for(var i = 0; i < walls.length; i++){
              console.log('iii', walls[i]);
              list += '<div data-id="'+walls[i].id+'" style="background: '+walls[i].color+'" class="add-column-item-wall">'+walls[i].name+'</div>'
            }
            var tooltip_content = '<div class="add-column-list-wall">'+list+'</div>';
            handler(tooltip_content);
          }

        }
        function fnErrorFn(data, status, headers, config) {
          console.log(data, status, headers, config);
          toastr.error(data.error);
        }

      function handler(tooltip_content){
        console.log('saddColum');
        tooltip.find('.tooltip-content').html(tooltip_content)
        
        
  
        /*$timeout(function () {
            tooltip.remove();
        }, 10000);*/
      }

    }

    $scope.addHashtag = function($event, hashtags){
      var el = $($event.currentTarget),
          target = $($event.target),
          block = el.parent().find('.item-block-hastags'),
          prt = el.parents('.item-hashtags'),
          input = el.find('.hashtag-input');

      
      input.val('');    
      if((target.hasClass('lattice') || target.hasClass('plus')) && ! el.hasClass('none-many')){
        el.toggleClass('active');
        el.removeClass('active-add');
      }

      if(el.hasClass('active')){
        input.focus();
        console.log("hashtags", hashtags)

        input.unbind().keyup(function(e){
          var e = $(e.currentTarget),
              value = e.val();
          e.val(value.replace(/\s/g, ''));      
          
          console.log('inputAddColumn', e);
          if(value.length>=3 && value.length<=20){
            var next = true;
            for(var i = 0; i < hashtags.length; i++){
              if(value.toLowerCase() == hashtags[i].name.toLowerCase()){
                next = false;
              }
            }
            if(next){
              el.addClass('active-add');
            }
            else{
              el.removeClass('active-add');
            }
            
          }
          else{
            el.removeClass('active-add');
          }
        });

        prt.find('.hashtag-add-b').unbind().click(function(ev){
          console.log("click");
          var value = prt.find('.hashtag-input').val(),
              id = prt.data('id');
          Column.add_hashtag(value, id).then(fnSuccessFn, fnErrorFn);

          function fnSuccessFn(data, status, headers, config) {
            var d = data.data;
            if(d.failure){
              toastr.error(d.failure);
            }
            else{ 
              hashtags.push(d.success);
              el.removeClass('active').removeClass('active-add');
            }
            console.log("add_hashtag", data)
          }
          function fnErrorFn(data, status, headers, config) {
            console.log(data, status, headers, config);
            toastr.error(data.error);
          }

        });
        

      }
      
      console.log('$scope.addHashtag', target)
    }

    $scope.hashtagTo = function(hashtag){
      console.log("hashtag",hashtag);
      $location.path("/hashtag/"+ hashtag.name.toLowerCase());//.replace().reload(false)
    }


    console.log("$scope.userType", $scope.userType);

     

  }]);
