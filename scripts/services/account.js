angular.module('contterApp')
  .factory('Account', function($http) {
    return {
      getProfile: function() {
        return $http.get('/api/me');
      },
      getUser: function(username) {
        return $http.post('/api/v1/user/get/', {
          username: username
        });
      },
      updateProfile: function(profileData) {
        return $http.put('/api/me', profileData);
      },
      getColumnProfile: function(page, sort) {
        return $http.post('/api/v1/me/column/', {
          page: page,
          sort: sort
        });
      },
      followProfile: function(id, action) {
        return $http.post('/api/v1/user/follow/', {
          id: id,
          action: action
        });
      },
      settingsTypesProfile: function(types) {
        return $http.post('/api/v1/user/settings/types/', {
          types: types
        });
      },
      settingsColorProfile: function(color) {
        return $http.post('/api/v1/user/settings/color/', {
          color: color
        });
      },
      settingsPictureProfile: function(image) {
        console.log(',settingsPictureProfile base', image);
        var formData = new FormData();
        formData.append('image', image);
        return $http({method: 'POST', url: '/api/v1/user/settings/picture/',
                         data: formData,
                         headers: {'Content-Type': undefined},
                         transformRequest: angular.identity})
        /*return $http({
          method: 'POST',
          url: '/api/v1/user/settings/picture/',
          headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: {
              image: image
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        });*/
        /*return $http({
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: {
                upload: image
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        })*/
      },
      getSettingsNotiProfile: function() {
        return $http.post('/api/v1/user/get/settings/notifications/', {
          
        });
      }
    };
  });